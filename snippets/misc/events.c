/*

  $Id$

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Gnokii is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Gnokii is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with gnokii; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (C) 2006 by Daniele Forsi

  Prints as vCalendar VEVENTs the call-related events stored in the phone.

  compile with
  gcc -Wall -o events events.c `pkg-config --libs gnokii`

  usage:
  events --all

*/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

#include <gnokii.h>

/* prepare for i18n */
#define _(x) x

struct gn_statemachine *state = NULL;

void busterminate(void) {
	gn_lib_phone_close(state);
	gn_lib_phoneprofile_free(&state);
	gn_lib_library_free();
}

void businit(void) {
	gn_error	error;
	
	atexit(busterminate);
	
	error = gn_lib_phoneprofile_load(NULL, &state);
	if (GN_ERR_NONE == error) {
		error = gn_lib_phone_open(state);
	}
	
	if (GN_ERR_NONE != error) {
		fprintf(stderr, "%s\n", gn_error_print(error));
		exit(-1);
	}
}

void signal_handler(int signal) {
	(void)signal;
	exit(-2);
}

void usage(char *progname) {
	fprintf(stderr, _("Usage: %s --all [--no-dialled-calls] [--no-missed-calls] [--no-received-calls]\n"), progname);
	fprintf(stderr, _("   or: %s [--dialled-calls] [--missed-calls] [--received-calls]\n"), progname);
	fprintf(stderr, _("Purpose: write to stdout as vCalendar VEVENTs the events stored in the phone\n"));
	fprintf(stderr, _("\t--all\t\t - write all the following\n"));
	fprintf(stderr, _("\t--dialled-calls\t - calls in DC memory\n"));
	fprintf(stderr, _("\t--missed-calls\t - calls in MC memory\n"));
	fprintf(stderr, _("\t--received-calls - calls in RC memory\t\n"));
	exit(-1);
}

void my_perror(const char *s, gn_error error) {
	if (error != GN_ERR_NONE) {
		if (s && *s) {
			fprintf(stderr, "%s: ", s);
		}
		fprintf(stderr, "%s\n", gn_error_print(error));
	}
}

int main(int argc, char *argv[]) {
	gn_data		data;
	gn_error	error = GN_ERR_NONE;

	gn_phonebook_entry pb_entry;
	int		dump_dialled_calls = 0;
	int		dump_missed_calls = 0;
	int		dump_received_calls = 0;

	int		i;

	/* read command line arguments */
	if (argc < 2) {
		usage(argv[0]);
	} else {
		for (i = 1; i < argc; i++) {
			if (!strcmp(argv[i], "--all")) {
				dump_dialled_calls = 1;
				dump_missed_calls = 1;
				dump_received_calls = 1;
			} else if (!strcmp(argv[i], "--dialled-calls")) {
				dump_dialled_calls = 1;
			} else if (!strcmp(argv[i], "--no-dialled-calls")) {
				dump_dialled_calls = 0;
			} else if (!strcmp(argv[i], "--missed-calls")) {
				dump_missed_calls = 1;
			} else if (!strcmp(argv[i], "--no-missed-calls")) {
				dump_missed_calls = 0;
			} else if (!strcmp(argv[i], "--received-calls")) {
				dump_received_calls = 1;
			} else if (!strcmp(argv[i], "--no-received-calls")) {
				dump_received_calls = 0;
			} else {
				usage(argv[0]);
			}
		}
	}

	/* init phone connection */

	businit();
	
	signal(SIGINT, signal_handler);

	gn_data_clear(&data);

	/* main loop start */

	if (dump_dialled_calls) {
		error = GN_ERR_NONE;
		pb_entry.memory_type = GN_MT_DC;
		data.phonebook_entry = &pb_entry;
		for (pb_entry.location = 0; ; pb_entry.location++) {
			error = gn_sm_functions(GN_OP_ReadPhonebook, &data, state);
			if (error == GN_ERR_NONE) {
				if (!pb_entry.empty) {
					gn_file_phonebook_raw_write(stdout, &pb_entry, "DC");
				}
			} else if (error == GN_ERR_EMPTYLOCATION || (error == GN_ERR_INVALIDLOCATION && pb_entry.location > 0)) {
				break;
			} else {
				my_perror("dump_dialled_calls", error);
			}
		}
	}

	if (dump_missed_calls) {
		error = GN_ERR_NONE;
		pb_entry.memory_type = GN_MT_MC;
		data.phonebook_entry = &pb_entry;
		for (pb_entry.location = 0; ; pb_entry.location++) {
			error = gn_sm_functions(GN_OP_ReadPhonebook, &data, state);
			if (error == GN_ERR_NONE) {
				if (!pb_entry.empty) {
					gn_file_phonebook_raw_write(stdout, &pb_entry, "MC");
				}
			} else if (error == GN_ERR_EMPTYLOCATION || (error == GN_ERR_INVALIDLOCATION && pb_entry.location > 0)) {
				break;
			} else {
				my_perror("dump_missed_calls", error);
			}
		}
	}

	if (dump_received_calls) {
		error = GN_ERR_NONE;
		pb_entry.memory_type = GN_MT_RC;
		data.phonebook_entry = &pb_entry;
		for (pb_entry.location = 0; ; pb_entry.location++) {
			error = gn_sm_functions(GN_OP_ReadPhonebook, &data, state);
			if (error == GN_ERR_NONE) {
				if (!pb_entry.empty) {
					gn_file_phonebook_raw_write(stdout, &pb_entry, "RC");
				}
			} else if (error == GN_ERR_EMPTYLOCATION || (error == GN_ERR_INVALIDLOCATION && pb_entry.location > 0)) {
				break;
			} else {
				my_perror("dump_received_calls", error);
			}
		}
	}

	/* main loop end */

	/* atexit() will terminate phone connection */

	//fprintf(stdout, _("Events successfully written: %d\n"), notes);

	return error;
}
