/*

  $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2006 by Daniele Forsi

  This prints a reference table with memory names from libgnokii.

  Compile and test:
  gcc -Wall memory_names.c -o memory_names $(pkg-config --libs gnokii) && ./memory_names

*/

#include <stdio.h>
#include <libintl.h>
#include <locale.h>

#include <gnokii.h>

#define _(x) gettext(x)

int main(void) {

	setlocale(LC_ALL, "");
	textdomain("gnokii");

#define macro_text(x) printf("%s %s\n", x, #x, _(gn_memory_type_print(GN_MT_##x)))
#define macro_html(x) printf("<tr><td>%s<td>%s\n", #x, _(gn_memory_type_print(GN_MT_##x)))
#define macro_php(x) printf("%s => '%s',\n", #x, _(gn_memory_type_print(GN_MT_##x)))

#define macro(x) macro_html(x)

	macro(ME);
	macro(SM);
	macro(FD);
	macro(ON);
	macro(EN);
	macro(DC);
	macro(RC);
	macro(MC);
	macro(LD);
	macro(BD);
	macro(SD);
	macro(MT);
	macro(TA);
	macro(CB);
	macro(IN);
	macro(OU);
	macro(AR);
	macro(TE);
	macro(SR);
	macro(DR);
	macro(OUS);
	macro(F1);
	macro(F2);
	macro(F3);
	macro(F4);
	macro(F5);
	macro(F6);
	macro(F7);
	macro(F8);
	macro(F9);
	macro(F10);
	macro(F11);
	macro(F12);
	macro(F13);
	macro(F14);
	macro(F15);
	macro(F16);
	macro(F17);
	macro(F18);
	macro(F19);
	macro(F20);
	macro(XX);

	return 0;
}
