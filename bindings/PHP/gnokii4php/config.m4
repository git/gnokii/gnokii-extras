dnl $Id$

PHP_ARG_ENABLE(gnokii, whether to enable gnokii support,
[  --enable-gnokii	Enable gnokii support])

if test "$PHP_GNOKII" = "yes"; then
  AC_DEFINE(HAVE_GNOKII, 1, [Whether you have gnokii])
  LDFLAGS="$(pkg-config --libs gnokii)"
  CFLAGS="$(pkg-config --cflags gnokii)"
  PHP_NEW_EXTENSION(gnokii, gnokii.c libgnokii_const.c file.c monitor.c other.c phonebook.c sms.c, $ext_shared)
fi

