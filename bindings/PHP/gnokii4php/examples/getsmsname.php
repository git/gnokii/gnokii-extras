#!/usr/bin/php
<?php // $Id$

/* clone of gnokii --getsms with sender name */

require_once 'common.php';

/*
 * Search in the phonebook the name corresponding to the given number
 */
function get_name($number) 
{
	global $phonebook;

	if (isset($phonebook[$number]))
		return $phonebook[$number]['name'];

	while ($phonebook['current_memory_type_idx'] < count($phonebook['memory_type'])) {
		$memory_type = $phonebook['memory_type'][$phonebook['current_memory_type_idx']];
		while ($phonebook['current_location'] <= $phonebook['last_location']) {
			$pe = gnokii_getphonebook($memory_type, $phonebook['current_location']);
			$phonebook['current_location']++;
			if ($pe && $pe['number']) {
				/* The same number can be found in more than one memory, keep only the first instance */
				if (!isset($phonebook[$pe['number']]))
					$phonebook[$pe['number']] = $pe;
				/* Quit as soon as the number is found, will continue from next entry if needed when called again */
				if ($number == $pe['number'])
					return $pe['name'];
			} else {
				/* An error happened, ignore it to continue with next memory type */
				break;
			}
		}
		$phonebook['current_location'] = 1;
		$phonebook['current_memory_type_idx']++;
	}
	return false;
}

function get_sms($memory_type, $start_number, $end_number)
{
	$no_error = array('code' => GN_ERR_NONE, 'string' => '');
	$items_read = 0;
	$messages = array();

	$error = $no_error;
	for ($number = $start_number; $number <= $end_number; $number++) {
		$sms = gnokii_getsms($memory_type, $number);

		if ($sms) {
			$items_read++;
			$remote_name = get_name($sms['remote_number']);
			$sms['remote_name'] = $remote_name;
			$messages[] = $sms;
		} else {
			$error = gnokii_lasterror();
			/* when reading only one entry always return the error */
			if ($start_number == $end_number) {
				return $error;
			}
			/* when reading more than one entry ignore empty locations
			 * but if all locations are empty the last $error is returned to the caller
			*/
			if ($error['code'] == GN_ERR_EMPTYLOCATION) {
				continue;
			}
			/* when reading to 'end' stop at the first invalid location */
			if (($error['code'] == GN_ERR_INVALIDLOCATION) && ($end_number == PHP_INT_MAX)) {
				if ($items_read == 0) {
					return array('code' => GN_ERR_EMPTYLOCATION, 'string' => 'All locations are empty');
				}
				/* ignore the error if at least one location was read successfully */
				if ($items_read > 0) {
					$error = $no_error;
					break;
				}
			}
			/* all other errors are fatal */
			return $error;
		}
	}
	$error['messages'] = $messages;
	return $error;
}

script_init();

/* get arguments */

if (($argc != 3) && ($argc != 4)) {
	echo "Usage: {$argv[0]} [--config config] [--phone phone] memory_type start [end]" . PHP_EOL;
	exit(1);
}
$memory_type = $argv[1];
$start_number = $argv[2];
if ($argc == 3) {
	$end_number = $start_number;
} elseif ($argv[3] == 'end') {
	$end_number = PHP_INT_MAX;
} else {
	$end_number = $argv[3];
}

$phonebook = array(
	'memory_type' => array('ME', 'SM'), /* Preferred memory first */
	'current_memory_type_idx' => 0,
	'current_location' => 1,
	'last_location' => 2000, /* Do not use PHP_INT_MAX because some phones don't return error for invalid phonebook locations */
	/* This could be preloaded with phonebook data read from a file, see get_name() above for format */
);
$error = get_sms($memory_type, $start_number, $end_number);

foreach ($error['messages'] as $sms) {
	echo "{$sms['number']}.\n";
	if ($sms['remote_name'])
		echo "From: {$sms['remote_name']} {$sms['remote_number']}\n";
	else
		echo "From: {$sms['remote_number']}\n";
	echo "Date: {$sms['datetime']}\n";
	echo "Text: {$sms['text']}\n";
	echo "\n";
}

print_gnokii_error($error);

script_terminate();

exit($error['code']);

