<?php // $Id$

/* code common to all examples */

function print_gnokii_error($error = false) {
	if (!$error) {
		$error = gnokii_lasterror();
	}
	if ($error['code'] != GN_ERR_NONE) {
		echo "Error ${error['code']}: ${error['string']}" . PHP_EOL;
	}
	return $error;
}

function load_extension($extension) {
	/* If the extension hasn't been loaded by php.ini try to load it now (using dl() will fail for SAPI PHP) */
	if (!extension_loaded($extension) && !dl($extension . '.' . PHP_SHLIB_SUFFIX)) {
		echo "Unable to load $extension extension" . PHP_EOL;
		return FALSE;
	}

	return TRUE;
}

function print_banner($extra_text = '') {
	$version = gnokii_version();
	echo "GNOKII Extension for PHP {$version['extension_version']}" . PHP_EOL;
	if ($extra_text) echo $extra_text . PHP_EOL;
}

function script_init($banner = '', $open = true) {
	global $argc, $argv;

	setlocale(LC_ALL, '');
	$config = '';
	$phone = '';

	/* gnokii functions and constants are available only if this call succeeds */
	if (!load_extension('gnokii')) exit(1);

	print_banner($banner);

	if ($argc > 2 && $argv[1] == '--config') {
		$config = $argv[2];
		$argc -= 2;
		array_splice($argv, 1, 2);
	}
	if ($argc > 2 && $argv[1] == '--phone') {
		$phone = $argv[2];
		$argc -= 2;
		array_splice($argv, 1, 2);
	}

	if ($open && !gnokii_open($config, $phone)) {
		echo 'Unable to open phone' . PHP_EOL;
		exit(1);
	}

	return TRUE;
}

function script_terminate() {
	return gnokii_close();
}

