/*

  $Id$

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Gnokii is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Gnokii is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with gnokii; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (C) 2007, 2008 by Daniele Forsi

  An utility program to convert phonebook formats using libgnokii functions.
  This code is inspired by gnokii-phonebook.c

Example:

phonebook_convert --vcard --ldif <infile >outfile

*/

#include <stdio.h>
#include <string.h>

#include <gnokii.h>

#define TYPE_NONE  0
#define TYPE_LDIF  1
#define TYPE_RAW   2
#define TYPE_VCARD 3

#define MAX_INPUT_LINE_LEN 1024

/* prepare for i18n */
#define _(x) x

int str2type(const char *str) {
	if (!strcmp(str, "--ldif")) {
		return TYPE_LDIF;
	} else if (!strcmp(str, "--raw")) {
		return TYPE_RAW;
	} else if (!strcmp(str, "--vcard")) {
		return TYPE_VCARD;
	}

	return TYPE_NONE;
}

int main(int argc, char *argv[]) {
	gn_error error = GN_ERR_NONE;
	int from_type, to_type, c;
	gn_phonebook_entry entry;
	char buffer[MAX_INPUT_LINE_LEN];

	if ((argc != 3) ||
		(TYPE_NONE == (from_type = str2type(argv[1]))) ||
		(TYPE_NONE == (to_type = str2type(argv[2])))
	) {
		fprintf(stderr, _("Usage: %s type1 type2 <infile >outfile\n"), argv[0]);
		fprintf(stderr, _("Where type is one of --ldif, --raw or --vcard.\n"));
		return -1;
	}

	while (!feof(stdin)) {
		/* set default values which can be overriden by the conversion functions */
		memset(&entry, 0, sizeof(gn_phonebook_entry));

		switch (from_type) {
		case TYPE_LDIF:
			if (gn_ldif2phonebook(stdin, &entry))
				error = GN_ERR_WRONGDATAFORMAT;
			break;
		case TYPE_RAW:
			if (gn_line_get(stdin, buffer, MAX_INPUT_LINE_LEN))
				error = gn_file_phonebook_raw_parse(&entry, buffer);
			break;
		case TYPE_VCARD:
			if (gn_vcard2phonebook(stdin, &entry) < 0)
				error = GN_ERR_WRONGDATAFORMAT;
			break;
		}
		if (error != GN_ERR_NONE) break;

		/* add here code to validate the entry, if needed */

		switch (to_type) {
		case TYPE_LDIF:
			if (gn_phonebook2ldif(stdout, &entry))
				error = GN_ERR_UNKNOWN;
			break;
		case TYPE_RAW:
			snprintf(buffer, sizeof(buffer), "%s", gn_memory_type2str(entry.memory_type));
			error = gn_file_phonebook_raw_write(stdout, &entry, buffer);
			break;
		case TYPE_VCARD:
			snprintf(buffer, sizeof(buffer), "%s%d", gn_memory_type2str(entry.memory_type), entry.location);
			if (gn_phonebook2vcard(stdout, &entry, buffer) < 0)
				error = GN_ERR_UNKNOWN;
			break;
		}
		if (error != GN_ERR_NONE) break;

		/* gn_vcard2phonebook() gn_ldif2phonebook() return an error if input file contains trailing empty lines */
		do {
			c = fgetc(stdin);
		} while (c == '\r' || c == '\n');
		ungetc(c, stdin);
	}

	if (error != GN_ERR_NONE) {
		fprintf(stderr, "%s\n", gn_error_print(error));
		return error;
	}

	return 0;
}
