/*

  $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2006 by Daniele Forsi

  This prints a list of all connections supported by the currently installed libgnokii
  or checks if a given connection is supported.

  Compile and test:
  gcc -Wall get_supported_connection.c -o get_supported_connection $(pkg-config --libs gnokii)
  conn=bluetooth; get_supported_connection $conn || echo "Sorry $conn is not compiled in your libgnokii"

*/

#include <stdio.h>
#include <string.h>

#include <gnokii.h>

/* don't expect this to work across libgnokii releases */
/* copied from ./include/gnokii-internal.h to suppress warnings */
GNOKII_API gn_connection_type gn_get_connectiontype(const char *connection_type_string);

int main(int argc, char *argv[]) {
	int i;
	const char *conn;

	if (argc == 2) {
		if (!strcmp(argv[1], "-l")) {
			for (i = 0; ; i++) {
				conn = gn_lib_get_supported_connection(i);
				if (!conn) break;
				printf("%s\n", conn);
			}
		} else {
			if (gn_get_connectiontype(argv[1]) == GN_CT_NONE) return 1;
		}
	} else {
		printf("Usage: %s -l|conn\n  -l    list all supported connection types\n  conn  connection type to search\n", argv[0]);
		return 1;
	}

	return 0;
}
