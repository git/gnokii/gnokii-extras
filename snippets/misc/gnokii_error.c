/*

  $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2006 by Daniele Forsi

  Prints the error message associated to the given error number.

  Compile and test:
  gcc -Wall gnokii_error.c -o gnokii_error $(pkg-config --libs gnokii) && ./gnokii_error 42

*/

#include <stdio.h>
#include <stdlib.h>

#include <gnokii.h>

int main(int argc, char *argv[]) {

	if ((argc != 2)) {
		printf("Usage %s number\n", argv[0]);
		return 1;
	}
	printf("%s\n", gn_error_print(atoi(argv[1])));

	return 0;
}
