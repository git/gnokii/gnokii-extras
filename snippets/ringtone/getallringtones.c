/*

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Gnokii is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Gnokii is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with gnokii; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (C) 2012 by Daniele Forsi

  Tries to read all ringtones from 0 to 999 and saves them in raw format.
  This doesn't use GN_OP_GetRingtoneList because it only shows location 100
  on Series 40 3rd, as of gnokii 0.6.31, but more ringtones are available.

  compile with
  gcc -Wall -o  getallringtones  getallringtones.c `pkg-config --libs gnokii`

  usage:
  getallringtones --overwrite

*/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

#include <gnokii.h>

/* prepare for i18n */
#define _(x) x

struct gn_statemachine *state = NULL;

void busterminate(void) {
	gn_lib_phone_close(state);
	gn_lib_phoneprofile_free(&state);
	gn_lib_library_free();
}

void businit(void) {
	gn_error	error;

	atexit(busterminate);

	error = gn_lib_phoneprofile_load(NULL, &state);
	if (GN_ERR_NONE == error) {
		error = gn_lib_phone_open(state);
	}

	if (GN_ERR_NONE != error) {
		fprintf(stderr, "%s\n", gn_error_print(error));
		exit(-1);
	}
}

void signal_handler(int signal) {
	(void)signal;

	exit(-2);
}

int main(int argc, char *argv[]) {
	gn_data		data;
	gn_error	error;
	gn_ringtone	ringtone;
	gn_raw_data	rawdata;
	unsigned char	buff[65536];
	FILE		*fp;

	if (argc != 2 || strcmp(argv[1], "--overwrite")) {
		fprintf(stderr, _("Usage: %s --overwrite\nPurpose: print a list of ringtones and write each one in a file\n"), argv[0]);
		fprintf(stderr, _("WARNING: files with the same name are overwritten without asking\n"));
		exit(-1);
	}

	businit();

	signal(SIGINT, signal_handler);

	memset(&ringtone, 0, sizeof(ringtone));
	rawdata.data = buff;
	gn_data_clear(&data);
	data.ringtone = &ringtone;
	data.raw_data = &rawdata;

	printf(_("#\tbytes\tname\n"));
	for (ringtone.location = 1; ringtone.location < 1000; ringtone.location++) {
		/* Set length every time because it is overwritten in GN_OP_GetRawRingtone */
		rawdata.length = sizeof(buff);
		error = gn_sm_functions(GN_OP_GetRawRingtone, &data, state);
		switch (error) {
		case GN_ERR_NONE:
			printf(_("%3d\t%5d\t%s\n"), ringtone.location, rawdata.length, ringtone.name);
			fp = fopen(ringtone.name, "w");
			if (fp) {
				fwrite(rawdata.data, rawdata.length, 1, fp);
				fclose(fp);
			} else {
				perror(argv[0]);
			}
			break;
		case GN_ERR_INVALIDSIZE:
			printf(_("%3d\t%5d\t*** %s\n"), ringtone.location, rawdata.length, gn_error_print(error));
			break;
		case GN_ERR_INVALIDLOCATION:
			/* Always ignore? On Nokia 5300 locations 80-98 are invalid but 99 and 100 are valid */
			break;
		default:
			break;
		}
	}

	exit(GN_ERR_NONE);
}
