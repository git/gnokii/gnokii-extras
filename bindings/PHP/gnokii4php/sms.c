/* $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2006, 2008, 2009 by Daniele Forsi

  SMS related functions for gnokii PHP extension.

*/

#include "php_gnokii.h"

ZEND_DECLARE_MODULE_GLOBALS(gnokii)

PHP_FUNCTION(gnokii_getsmsc)
{
	long location;
	gn_data data;
	gn_sms_message_center message_center;
	gn_error error;
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &location) == FAILURE) {
		RETURN_NULL();
	}

	gn_data_clear(&data);
	memset(&message_center, 0, sizeof(message_center));
	data.message_center = &message_center;

	message_center.id = location;
	error = gn_sm_functions(GN_OP_GetSMSCenter, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	array_init(return_value);
	add_assoc_long(return_value, "location", message_center.id);
	add_assoc_string(return_value, "name", message_center.name, 1);
	add_assoc_string(return_value, "smsc_number", message_center.smsc.number, 1);
	add_assoc_long(return_value, "smsc_type", message_center.smsc.type);
	add_assoc_string(return_value, "recipient_number", message_center.recipient.number, 1);
	add_assoc_long(return_value, "recipient_type", message_center.recipient.type);
	add_assoc_long(return_value, "format", message_center.format);
	add_assoc_long(return_value, "validity", message_center.validity);
}

PHP_FUNCTION(gnokii_getsmsstatus)
{
	gn_data data;
	gn_sms_status smsstatus = {0, 0, 0, 0};
	gn_error error;
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "") == FAILURE) {
		RETURN_NULL();
	}

	gn_data_clear(&data);
	data.sms_status = &smsstatus;
	error = gn_sm_functions(GN_OP_GetSMSStatus, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	array_init(return_value);
	add_assoc_long(return_value, "unread", smsstatus.unread);
	add_assoc_long(return_value, "number", smsstatus.number);
}

PHP_FUNCTION(gnokii_getsms)
{
	char *memory_type_string;
	int memory_type_len;
	long location;
	gn_memory_type memory_type;
	gn_data data;
	gn_sms message;
	gn_sms_folder folder;
	gn_sms_folder_list folderlist;
	gn_error error;
	char datebuf[27];
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sl", &memory_type_string, &memory_type_len, &location) == FAILURE) {
		RETURN_NULL();
	}

	memory_type = gn_str2memory_type(memory_type_string);
	if (memory_type == GN_MT_XX) {
		LASTERROR(state, GN_ERR_INVALIDMEMORYTYPE);
		RETURN_NULL();
	}

	gn_data_clear(&data);
	memset(&message, 0, sizeof(gn_sms));
	message.memory_type = memory_type;
	message.number = location;
	data.sms = &message;

	/* some drivers need these structs */
	folder.folder_id = 0;
	data.sms_folder = &folder;
	data.sms_folder_list = &folderlist;

	error = gn_sms_get(&data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	array_init(return_value);
	add_assoc_string(return_value, "remote_number", message.remote.number, 1);
	add_assoc_string(return_value, "text", message.user_data[0].u.text, 1);
	add_assoc_long(return_value, "type", message.type);
	add_assoc_long(return_value, "number", message.number);

	if (!message.udh.number) message.udh.udh[0].type = GN_SMS_UDH_None;
	switch (message.udh.udh[0].type) {
	case GN_SMS_UDH_ConcatenatedMessages:
		add_assoc_long(return_value, "reference_number", message.udh.udh[0].u.concatenated_short_message.reference_number);
		add_assoc_long(return_value, "current_number", message.udh.udh[0].u.concatenated_short_message.current_number);
		add_assoc_long(return_value, "maximum_number", message.udh.udh[0].u.concatenated_short_message.maximum_number);
		break;
	}

	/* do NOT translate this date format */
	snprintf(datebuf, sizeof(datebuf), "%02d/%02d/%04d %02d:%02d:%02d %+03d00", \
		message.smsc_time.day, message.smsc_time.month, message.smsc_time.year, \
		message.smsc_time.hour, message.smsc_time.minute, message.smsc_time.second, \
		message.smsc_time.timezone);
	datebuf[sizeof(datebuf)-1] = '\0';
	add_assoc_string(return_value, "datetime", datebuf, 1);
}

PHP_FUNCTION(gnokii_deletesms)
{
	char *memory_type_string;
	int memory_type_len;
	long location;
	gn_memory_type memory_type;
	gn_data data;
	gn_sms message;
	gn_sms_folder folder;
	gn_sms_folder_list folderlist;
	gn_error error;
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sl", &memory_type_string, &memory_type_len, &location) == FAILURE) {
		RETURN_NULL();
	}

	memory_type = gn_str2memory_type(memory_type_string);
	if (memory_type == GN_MT_XX) {
		LASTERROR(state, GN_ERR_INVALIDMEMORYTYPE);
		RETURN_NULL();
	}

	gn_data_clear(&data);
	memset(&message, 0, sizeof(gn_sms));
	message.memory_type = memory_type;
	message.number = location;
	data.sms = &message;

	/* some drivers need these structs */
	folder.folder_id = 0;
	data.sms_folder = &folder;
	data.sms_folder_list = &folderlist;

	error = gn_sms_delete(&data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	RETURN_TRUE;
}

PHP_FUNCTION(gnokii_sendsms)
{
	char *number, *text;
	int number_len, text_len, j;
	struct gn_statemachine *state;
	gn_sms sms;
	gn_data data;
	gn_error error;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ss", &number, &number_len, &text, &text_len) == FAILURE) {
		RETURN_NULL();
	}
	if (number_len >= sizeof(sms.remote.number)) {
		RETURN_NULL();
	}
	if (text_len > 255 * 153) {
		RETURN_NULL();
	}

	/* The memory is zeroed here */
	gn_sms_default_submit(&sms);

	snprintf(sms.remote.number, sizeof(sms.remote.number), "%s", number);
	if (sms.remote.number[0] == '+')
		sms.remote.type = GN_GSM_NUMBER_International;
	else
		sms.remote.type = GN_GSM_NUMBER_Unknown;

	memcpy(sms.user_data[0].u.text, text, text_len);
	sms.user_data[0].length = text_len;

	sms.user_data[0].type = GN_SMS_DATA_Text;
	if (!gn_char_def_alphabet(sms.user_data[0].u.text))
		sms.dcs.u.general.alphabet = GN_SMS_DCS_UCS2;
	sms.user_data[1].type = GN_SMS_DATA_None;

	gn_data_clear(&data);
	data.sms = &sms;

	error = gn_sms_send(&data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	array_init(return_value);
	for (j = 0; j < sms.parts; j++) {
		add_next_index_long(return_value, sms.reference[j]);
	}
}
