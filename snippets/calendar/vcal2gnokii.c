/*

  $Id$

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Gnokii is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Gnokii is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with gnokii; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (C) 2006 by Daniele Forsi

  Read the first vCalendar entry of each file and import it either as
  a Calendar Note or as a ToDo Note.

  compile with
  gcc -Wall -o vcal2gnokii vcal2gnokii.c `pkg-config --libs gnokii`

  usage:
  vcal2gnokii --calendar vcalendarfile1 vcalendarfile2 .. vcalendarfileN
  or:
  vcal2gnokii --todo vcalendarfile1 vcalendarfile2 .. vcalendarfileN

*/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

#include <gnokii.h>

/* prepare for i18n */
#define _(x) x

struct gn_statemachine *state = NULL;

void busterminate(void) {
	gn_lib_phone_close(state);
	gn_lib_phoneprofile_free(&state);
	gn_lib_library_free();
}

void businit(void) {
	gn_error	error;
	
	atexit(busterminate);
	
	error = gn_lib_phoneprofile_load(NULL, &state);
	if (GN_ERR_NONE == error) {
		error = gn_lib_phone_open(state);
	}
	
	if (GN_ERR_NONE != error) {
		fprintf(stderr, "%s\n", gn_error_print(error));
		exit(-1);
	}
}

void signal_handler(int signal) {
	(void)signal;
	exit(-2);
}

void usage(char *progname) {
	fprintf(stderr, _("Usage: %s --calendar|--todo file...\n"), progname);
	fprintf(stderr, _("Purpose: write all vCalendar entries read from stdin as calendar or todo notes\n"));
	exit(-1);
}

int main(int argc, char *argv[]) {
	gn_data		data;
	gn_error	error = GN_ERR_NONE;
	gn_calnote	calnote;
	gn_calnote_list	calnotelist;
	gn_todo		todo;
	int		notes;
	int		use_calendar;
	int		filename;
	FILE		*fp;

	if (argc < 2) {
		usage(argv[0]);
	} else {
		if (!strcmp(argv[1], "--calendar")) {
			use_calendar = 1;
		} else if (!strcmp(argv[1], "--todo")) {
			use_calendar = 0;
		} else {
			usage(argv[0]);
		}
	}

	businit();
	
	signal(SIGINT, signal_handler);

	gn_data_clear(&data);

	notes = 0;
	for (filename = 2; filename < argc; filename++) {
		fp = fopen(argv[filename], "r");
		if (!fp) {
			fprintf(stderr, _("Can't open file %s\n"), argv[filename]);
			perror(NULL);
			error = GN_ERR_FAILED;
			break;
		}

		/* always read note number "1" */
		if (use_calendar) {
			error = gn_ical2calnote(fp, &calnote, 1);
		} else {
			error = gn_ical2todo(fp, &todo, 1);
		}
		fclose(fp);

		if (error == GN_ERR_NOTIMPLEMENTED) {
			if (use_calendar) {
				error = gn_vcal_file_event_read(argv[filename], &calnote, 1);
			} else {
				error = gn_vcal_file_todo_read(argv[filename], &todo, 1);
			}
/*
			fprintf(stderr,_("Sorry, libgnokii was compiled without libical support\n"));
			exit(error);
*/
		}

		if (error != GN_ERR_NONE) {
			fprintf(stderr, _("Failed to read note #%d from file %s: %s\n"), notes, argv[filename], gn_error_print(error));
			break;
		}

		if (use_calendar) {
			data.calnote = &calnote;
			data.calnote_list = &calnotelist;
			error = gn_sm_functions(GN_OP_WriteCalendarNote, &data, state);
		} else {
			data.todo = &todo;
			error = gn_sm_functions(GN_OP_WriteToDo, &data, state);
		}

		if (error != GN_ERR_NONE) {
			fprintf(stderr, _("Failed to write note #%d to phone: %s\n"), notes + 1, gn_error_print(error));
			/* quit immediately after the first error */
			break;
		} else {
			notes++;
			fprintf(stdout, _("Note #%d successfully written to phone\n"), notes);
		}
	}

	fprintf(stdout, _("Notes successfully written to phone: %d\n"), notes);

	return error;
}
