/*

  $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2006 by Daniele Forsi

  This prints a table with error codes and messages from libgnokii.

  Compile and test:
  gcc -Wall error_codes_intl.c -o error_codes_intl $(pkg-config --libs gnokii) && ./error_codes_intl

*/

#include <stdio.h>
#include <libintl.h>
#include <locale.h>
#define _(x) gettext(x)

#include <gnokii.h>

int main(void) {

	setlocale(LC_ALL, "");
	textdomain("gnokii");

#define macro_text(x) printf("%d %s %s\n", x, #x, _(gn_error_print(x)))
#define macro_html(x) printf("<tr><td>%d<td>%s<td>%s\n", x, #x, _(gn_error_print(x)))
#define macro_php(x) printf("%d => \"%s\",\n", x, _(gn_error_print(x)))

#define macro(x) macro_php(x)

	/* General codes */
	macro(GN_ERR_NONE);
	macro(GN_ERR_FAILED);
	macro(GN_ERR_UNKNOWNMODEL);
	macro(GN_ERR_INVALIDSECURITYCODE);
	macro(GN_ERR_INTERNALERROR);
	macro(GN_ERR_NOTIMPLEMENTED);
	macro(GN_ERR_NOTSUPPORTED);
	macro(GN_ERR_USERCANCELED);
	macro(GN_ERR_UNKNOWN);
	macro(GN_ERR_MEMORYFULL);

	/* Statemachine */
	macro(GN_ERR_NOLINK);
	macro(GN_ERR_TIMEOUT);
	macro(GN_ERR_TRYAGAIN);
	macro(GN_ERR_WAITING);
	macro(GN_ERR_NOTREADY);
	macro(GN_ERR_BUSY);
	
	/* Locations */
	macro(GN_ERR_INVALIDLOCATION);
	macro(GN_ERR_INVALIDMEMORYTYPE);
	macro(GN_ERR_EMPTYLOCATION);

	/* Format */
	macro(GN_ERR_ENTRYTOOLONG);
	macro(GN_ERR_WRONGDATAFORMAT);
	macro(GN_ERR_INVALIDSIZE);

	/* The following are here in anticipation of data call requirements. */
	macro(GN_ERR_LINEBUSY);
	macro(GN_ERR_NOCARRIER);

	/* The following value signals the current frame is unhandled */
	macro(GN_ERR_UNHANDLEDFRAME);
	macro(GN_ERR_UNSOLICITED);

	/* Other */
	macro(GN_ERR_NONEWCBRECEIVED);
	macro(GN_ERR_SIMPROBLEM);
	macro(GN_ERR_CODEREQUIRED);
	macro(GN_ERR_NOTAVAILABLE);

	/* Config */
	macro(GN_ERR_NOCONFIG);
	macro(GN_ERR_NOPHONE);
	macro(GN_ERR_NOLOG);
	macro(GN_ERR_NOMODEL);
	macro(GN_ERR_NOPORT);
	macro(GN_ERR_NOCONNECTION);

	macro(GN_ERR_ASYNC);

	return 0;
}
