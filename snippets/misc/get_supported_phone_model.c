/*

  $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2006 by Daniele Forsi

  This prints a list of all phones supported by xgnokii with the currently
  installed libgnokii.
  This list should contain all models supported by the drivers, but note that
  gnokii is not using this list, instead it asks each driver in turn.

  Compile and test:
  gcc -Wall get_supported_phone_model.c -o get_supported_phone_model $(pkg-config --libs gnokii)
  get_supported_phone_model --list

*/

#include <stdio.h>
#include <string.h>

#include <gnokii.h>

int main(int argc, char *argv[]) {
	int i;
	const char *model;

	if (argc == 2) {
		/* print full list */
		if ((!strcmp(argv[1], "-l")) || (!strcmp(argv[1], "--list"))) {
			for (i = 0; ; i++) {
				model = gn_lib_get_supported_phone_model(i);
				if (!model) break;
				printf("%s\n", model);
			}
			return 0;
		}
	}

	printf("Usage: %s -l|--list\n", argv[0]);
	printf("  -l|--list    list all phone models supported by xgnokii\n");

	return 1;
}
