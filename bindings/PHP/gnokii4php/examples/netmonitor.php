#!/usr/bin/php
<?php // $Id$

/* clone of gnokii --netmonitor */

require_once 'common.php';

function print_netmonitor($screen)
{
	$netmonitor = gnokii_netmonitor($screen);

	if ($netmonitor) {
		echo sprintf("Netmonitor menu %d received:\n", $netmonitor['field']);
		echo $netmonitor['screen'] . PHP_EOL;
	}

	return gnokii_lasterror();
}

/* get arguments */

script_init();

if ($argc != 2) {
	echo "Usage: {$argv[0]} [--config config] [--phone phone] {reset|off|field|devel|next|number}" . PHP_EOL;
	exit(1);
}
$screen = $argv[1];

$error = print_netmonitor($screen);

script_terminate();

exit($error['code']);

