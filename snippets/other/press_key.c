/*

  $Id$

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Gnokii is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Gnokii is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with gnokii; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (C) 2007 by Daniele Forsi

  Keep a key pressed for a length of time.

  compile with
  gcc -Wall -o press_key press_key.c `pkg-config --libs gnokii`

  press_key 4 P

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>

#include <gnokii.h>

/* prepare for i18n */
#define _(x) x

struct gn_statemachine *state = NULL;

void busterminate(void) {
	gn_lib_phone_close(state);
	gn_lib_phoneprofile_free(&state);
	gn_lib_library_free();
}

void businit(void) {
	gn_error	error;
	
	atexit(busterminate);
	
	error = gn_lib_phoneprofile_load(NULL, &state);
	if (GN_ERR_NONE == error) {
		error = gn_lib_phone_open(state);
	}
	
	if (GN_ERR_NONE != error) {
		fprintf(stderr, "%s\n", gn_error_print(error));
		exit(-1);
	}
}

void signal_handler(int signal) {
	(void)signal;
	exit(-2);
}

/* presskeysequence() was copied from gnokii/gnokii-other.c 1.5 and slightly modified */
gn_error presskeysequence(gn_data *data, struct gn_statemachine *state, char ch, int seconds)
{
	gn_error error = GN_ERR_NONE;
	char *syms = "0123456789#*PGR+-UDMN";
	gn_key_code keys[] = {GN_KEY_0, GN_KEY_1, GN_KEY_2, GN_KEY_3,
			      GN_KEY_4, GN_KEY_5, GN_KEY_6, GN_KEY_7,
			      GN_KEY_8, GN_KEY_9, GN_KEY_HASH,
			      GN_KEY_ASTERISK, GN_KEY_POWER, GN_KEY_GREEN,
			      GN_KEY_RED, GN_KEY_INCREASEVOLUME,
			      GN_KEY_DECREASEVOLUME, GN_KEY_UP, GN_KEY_DOWN,
			      GN_KEY_MENU, GN_KEY_NAMES};
	char *pos;

	gn_data_clear(data);

	if ((pos = strchr(syms, toupper(ch))) != NULL) {
		data->key_code = keys[pos - syms];
		error = gn_sm_functions(GN_OP_PressPhoneKey, data, state);
		if (error == GN_ERR_NONE) {
			sleep(seconds);
			error = gn_sm_functions(GN_OP_ReleasePhoneKey, data, state);
		}
	} else {
		fprintf(stderr, _("Supported keys are only: %s\n"), syms);
		error = GN_ERR_FAILED;
	}

	return error;
}

int main(int argc, char *argv[]) {
	gn_data		data;
	gn_error	error;
	int		seconds;
	char		key;

	if (argc != 3) {
		fprintf(stderr, _("Usage: %s {seconds} {key}\nPurpose: simulate keeping a single key pressed for a number of seconds\n"), argv[0]);
		exit(-1);
	}

	seconds = atoi(argv[1]);
	if (seconds < 1) {
		fprintf(stderr, _("Argument for {seconds} must be an integer greater than 0\n"));
		exit(-1);
	}

	if (strlen(argv[2]) != 1) {
		fprintf(stderr, _("Argument for {key} must be a single character\n"));
		exit(-1);
	}
	key = argv[2][0];

	businit();
	
	signal(SIGINT, signal_handler);

	error = presskeysequence(&data, state, key, seconds);

	exit(error);
}
