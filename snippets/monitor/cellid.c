/*

  $Id: cellid.c,v 1.3 2007/02/23 22:23:17 dforsi Exp $

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Gnokii is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Gnokii is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with gnokii; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (C) 2006 by Daniele Forsi

  Prints cellid-LAC-MNC-MCC.

  compile with
  gcc -Wall -o cellid cellid.c `pkg-config --libs gnokii`

  usage:
  cellid

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#include <gnokii.h>

/* prepare for i18n */
#define _(x) x

struct gn_statemachine *state = NULL;

void busterminate(void) {
	gn_lib_phone_close(state);
	gn_lib_phoneprofile_free(&state);
	gn_lib_library_free();
}

void businit(void) {
	gn_error	error;
	
	atexit(busterminate);
	
	error = gn_lib_phoneprofile_load(NULL, &state);
	if (GN_ERR_NONE == error) {
		error = gn_lib_phone_open(state);
	}
	
	if (GN_ERR_NONE != error) {
		fprintf(stderr, "%s\n", gn_error_print(error));
		exit(-1);
	}
}

void signal_handler(int signal) {
	(void)signal;
	exit(-2);
}

int main(int argc, char *argv[]) {
	gn_data		data;
	gn_error	error;
	gn_network_info	networkinfo;

	if (argc != 1) {
		fprintf(stderr, _("Usage: %s\nPurpose: print identification data of the current GSM cell\n"), argv[0]);
		fprintf(stderr, _("Format is CellID-LAC-MNC-MCC as decimal values\n"));
		exit(-1);
	}

	businit();

	signal(SIGINT, signal_handler);

	gn_data_clear(&data);
	memset(&networkinfo, 0, sizeof(networkinfo));
	data.network_info = &networkinfo;

	error = gn_sm_functions(GN_OP_GetNetworkInfo, &data, state);
	if (error == GN_ERR_NONE) {
		unsigned int cid = 0, lac = 0, mnc = 0, mcc = 0;

		/* Ugly, ugly, ... */
		if (networkinfo.cell_id[2] == 0 && networkinfo.cell_id[3] == 0)
			cid = (networkinfo.cell_id[0] << 8) + networkinfo.cell_id[1];
		else
			cid = (networkinfo.cell_id[0] << 24) + (networkinfo.cell_id[1] << 16) + (networkinfo.cell_id[2] << 8) + networkinfo.cell_id[3];	

		lac = (networkinfo.LAC[0] << 8) + networkinfo.LAC[1];

		if (strlen(networkinfo.network_code) > 3) {
			mcc = atoi(&networkinfo.network_code[0]);
			mnc = atoi(&networkinfo.network_code[4]);
		}

		printf("%d-%d-%d-%d\n", cid, lac, mnc, mcc);
		printf("CellID: %d LAC: %d MNC: %d MCC: %d\n", cid, lac, mnc, mcc);
	}

	exit(error);
}
