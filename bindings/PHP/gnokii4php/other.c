/* $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2008 by Daniele Forsi

  Other functions for gnokii PHP extension.

*/

#include "php_gnokii.h"

ZEND_DECLARE_MODULE_GLOBALS(gnokii)

PHP_FUNCTION(gnokii_listnetworks)
{
	gn_network network;
	int i = 0;
	zval *subarray;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "") == FAILURE) {
		RETURN_NULL();
	}

	array_init(return_value);
	while (gn_network_get(&network, i++)) {
		MAKE_STD_ZVAL(subarray);
		array_init(subarray);

		add_assoc_string(subarray, "code", network.code, 1);
		add_assoc_string(subarray, "name", network.name, 1);
		/* use a numeric index because neither network.code nor network.name are unique */
		add_next_index_zval(return_value, subarray);
	}
}

PHP_FUNCTION(gnokii_getnetworkinfo)
{
	gn_data data;
	gn_network_info networkinfo;
	gn_error error;
	struct gn_statemachine *state;
	int lac, cid;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "") == FAILURE) {
		RETURN_NULL();
	}

	gn_data_clear(&data);
	memset(&networkinfo, 0, sizeof(networkinfo));
	data.network_info = &networkinfo;
	error = gn_sm_functions(GN_OP_GetNetworkInfo, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	/* Ugly, ugly, ... */
	if (networkinfo.cell_id[2] == 0 && networkinfo.cell_id[3] == 0)  
		cid = (networkinfo.cell_id[0] << 8) + networkinfo.cell_id[1];
	else
		cid = (networkinfo.cell_id[0] << 24) + (networkinfo.cell_id[1] << 16) + (networkinfo.cell_id[2] << 8) + networkinfo.cell_id[3];
	lac = (networkinfo.LAC[0] << 8) + networkinfo.LAC[1];

	array_init(return_value);
	add_assoc_string(return_value, "code", networkinfo.network_code, 1);
	add_assoc_string(return_value, "name", gn_network_name_get(networkinfo.network_code), 1);
	add_assoc_string(return_value, "country", gn_country_name_get(networkinfo.network_code), 1);
	add_assoc_long(return_value, "lac", lac);
	add_assoc_long(return_value, "cell_id", cid);
}

