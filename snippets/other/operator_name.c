/*

  $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2006, 2007 by Daniele Forsi

  Prints the name of the operator corresponding to the given MCC and MNC.
  Prints "unknown" if the name is not found.

  gcc -Wall operator_name.c -o operator_name $(pkg-config --libs gnokii) && operator_name 222 88
*/

#include <stdlib.h>
#include <string.h>

#include <gnokii.h>

int main(int argc, char *argv[]) {
	char *mcc, *mnc;
	char code[8];

	/* check arguments */
	if (argc == 3) {
		mcc = argv[1];
		mnc = argv[2];
	} else {
		printf("Usage: %s mobile_country_code mobile_network_code\n", argv[0]);
		exit(1);
	}

	/* check mobile country code */
	if (strlen(mcc) != 3) {
		printf("MCC length must be 3\n");
		exit(1);
	}

	/* check mobile network code */
	if ((strlen(mnc) != 2) && (strlen(mnc) != 3)) {
		printf("MNC length must be 2 or 3\n");
		exit(1);
	}

	/* call libgnokii and print results */
	sprintf(code, "%3s %.3s", mcc, mnc);
	printf("%s (%s)\n", gn_network_name_get(code), gn_country_name_get(mcc));

	exit(0);
}
