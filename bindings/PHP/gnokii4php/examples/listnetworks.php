#!/usr/bin/php
<?php // $Id$

/* clone of gnokii --listnetworks (minus country name) */

require_once 'common.php';

function print_networks()
{
	$networks = gnokii_listnetworks();

	if (!$networks)
		return GN_ERR_FAILED;

	echo 'Network  Name' . PHP_EOL;
	echo '-----------------------------------------' . PHP_EOL;
	foreach ($networks as $network) {
		echo sprintf("%-7s  %s", $network['code'], $network['name']) . PHP_EOL;
	}
	return GN_ERR_NONE;
}

/* get arguments */

if ($argc != 1) {
	echo "Usage: {$argv[0]}" . PHP_EOL;
	exit(1);
}

/* no need to open the phone in this particular case */
script_init('', false);

$error_code = print_networks();

script_terminate();

exit($error_code);

