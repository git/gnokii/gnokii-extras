/*

  $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2006 by Daniele Forsi

  This prints a list of all networks known by the currently installed libgnokii

  Compile and test:
  gcc -Wall net_list_html.c -o net_list_html $(pkg-config --libs gnokii) && net_list_html

*/

#include <stdio.h>
#include <string.h>

#include <gnokii.h>

int main(void) {
	int i;
	gn_network network;
	char country_code[4] = {0,0,0,0};

	i = 0;
	while (gn_network_get(&network, i++)) {
		if (strncmp(country_code, network.code, 3)) {
			/* close previous list */
			if (country_code[0]) printf("</ul></ul>");

			country_code[0] = network.code[0];
			country_code[1] = network.code[1];
			country_code[2] = network.code[2];
			printf("<ul><li>%s %s<ul>", country_code, gn_country_name_get(country_code));
		}
		printf("<li>%s %s", network.code, network.name);
	}
	printf("</ul></ul>");

	return 0;
}
