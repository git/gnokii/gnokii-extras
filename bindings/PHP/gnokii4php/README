I. How To Compile and Install
II. Note About Apache
III. PHP Functions and Constants


I. How To Compile and Install

If you already have PHP and gnokii installed in your system, either uninstall
them or make sure you understand the implications of having two versions,
possibly with different features.
To avoid interference with currently installed versions, the examples below
use a configure --prefix to install the new versions *outside* of the path, in
the home directory, so there is no need to run make && sudo make install like
when installing in a system directory.
Use --prefix=/usr if you don't mind overwriting the versions installed by your
distribution.

1. Install libgnokii and its include files

You only need libgnokii and the include files, but consider also installing
gnokii so that you can compare your PHP programs with it.

On a debian-like distribution you can run:
  sudo apt-get install libgnokii3-dev
This will install also all other dependencies including libgnokii3.
To install gnokii:
  sudo apt-get install gnokii-cli

However since for obvious reasons stable distributions usually supply out of
date versions, you should download the latest gnokii sources from the source
repository, configure, compile and install (read the INSTALL file from gnokii
for the dependencies).
  ./configure --prefix=$HOME --enable-security
  make -C common install && make -C include install
Or, if you want to install everything:
  make install

2. Install PHP and its include files

On a debian-like distribution you can run:
  apt-get source php5-cli
Note that files are downloaded in the current directory.
If you prefer, get PHP4 or PHP5 sources from php.net.

In the PHP sources directory run:
  ./configure --prefix=$HOME --enable-discard-path
The --enable-discard-path option is not strictly necessary but it can increase
security if you use PHP as cgi.
Add --with-apxs to use PHP as a module within Apache.
If you want to debug this extension then configure PHP4 using
   --enable-debug --enable-experimental-zts
or configure PHP5 with
   --enable-debug --enable-maintainer-zts
Add any other option that you need

You can install everything (make install), but you just need:
  make install-programs
  make install-headers
  make install-build
  make install-cli

3. Install the gnokii4php extension

Compiling this version has been tested with PHP 4.4.4 and PHP 5.2.6 only after
installing PHP from source with and without debug options enabled.

If you added a --prefix at configure time of PHP, now you need to specify the
full path to phpize and pass the full filename of php-config using the
--with-php-config option of configure.

  $HOME/bin/phpize
  ./configure --enable-gnokii --with-php-config=$HOME/bin/php-config
  make install

4. Test

If you added a --prefix at configure time of PHP, now you need to specify the
full path to php:
  $HOME/bin/php -r 'dl("gnokii.so"); gnokii_open(); print_r( gnokii_identify() );'

5. Examples

See the examples/ directory.
Most of the code dealing with libgnokii is taken or inspired by gnokii so the
programs in the example directory should give results similar or identical to
commandline gnokii.


II. Note About Apache

You need to enable the extension in php.ini adding

extension=gnokii.so

in the section "Dynamic Extensions" because you cannot load it
using dl() inside the script in a threaded server.


III. PHP Functions and Constants

The following PHP functions are available (from php_gnokii.h):

 * gnokii_close
 * gnokii_deletefile
 * gnokii_deletephonebook
 * gnokii_deletesms
 * gnokii_getfile
 * gnokii_getfilelist
 * gnokii_getmemorystatus
 * gnokii_getnetworkinfo
 * gnokii_getphonebook
 * gnokii_getsms
 * gnokii_getsmsc
 * gnokii_getsmsstatus
 * gnokii_identify
 * gnokii_lasterror
 * gnokii_listnetworks
 * gnokii_netmonitor
 * gnokii_open
 * gnokii_putfile
 * gnokii_reset
 * gnokii_sendsms
 * gnokii_version

The following PHP constants are available (from libgnokii_const.c):

 * error constants starting with GN_ERR_*

$Id$
