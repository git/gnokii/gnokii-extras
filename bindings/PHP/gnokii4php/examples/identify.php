#!/usr/bin/php
<?php // $Id$

/* clone of gnokii --identify */

require_once 'common.php';

script_init();

$id = gnokii_identify();

echo 'IMEI         : ' . $id['imei'] . PHP_EOL;
echo 'Manufacturer : ' . $id['manufacturer'] . PHP_EOL;
echo 'Model        : ' . $id['model'] . PHP_EOL;
echo 'Product name : ' . $id['product_name'] . PHP_EOL;
echo 'Revision     : ' . $id['revision'] . PHP_EOL;

script_terminate();

