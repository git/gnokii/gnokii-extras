/* $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program); write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2006 by Daniele Forsi

  Make some gnokii defines available to PHP as constants.

*/

#include "php_gnokii.h"

int php_gnokii_const_init(int module_number) {
	TSRMLS_FETCH();

#define ADD_PHP_CONST(x) REGISTER_LONG_CONSTANT(#x, x, CONST_CS | CONST_PERSISTENT)
	
	/* error codes copied from error.h,v 1.23 2007/10/30 09:12:23 pkot */
	
	/* General codes */
	ADD_PHP_CONST(GN_ERR_NONE);			/* No error. */
	ADD_PHP_CONST(GN_ERR_FAILED);			/* Command failed. */
	ADD_PHP_CONST(GN_ERR_UNKNOWNMODEL);		/* Model specified isn't known/supported. */
	ADD_PHP_CONST(GN_ERR_INVALIDSECURITYCODE);	/* Invalid Security code. */
	ADD_PHP_CONST(GN_ERR_INTERNALERROR);		/* Problem occurred internal to model specific code. */
	ADD_PHP_CONST(GN_ERR_NOTIMPLEMENTED);		/* Command called isn't implemented in model. */
	ADD_PHP_CONST(GN_ERR_NOTSUPPORTED);		/* Function not supported by the phone */
	ADD_PHP_CONST(GN_ERR_USERCANCELED);		/* User aborted the action. */
	ADD_PHP_CONST(GN_ERR_UNKNOWN);			/* Unknown error - well better than nothing!! */
	ADD_PHP_CONST(GN_ERR_MEMORYFULL);		/* The specified memory is full. */
	
	/* Statemachine */
	ADD_PHP_CONST(GN_ERR_NOLINK);			/* Couldn't establish link with phone. */
	ADD_PHP_CONST(GN_ERR_TIMEOUT);			/* Command timed out. */
	ADD_PHP_CONST(GN_ERR_TRYAGAIN);			/* Try again. */
	ADD_PHP_CONST(GN_ERR_WAITING);			/* Waiting for the next part of the message. */
	ADD_PHP_CONST(GN_ERR_NOTREADY);			/* Device not ready. */
	ADD_PHP_CONST(GN_ERR_BUSY);			/* Command is still being executed. */
		
	/* Locations */
	ADD_PHP_CONST(GN_ERR_INVALIDLOCATION);		/* The given memory location has not valid location. */
	ADD_PHP_CONST(GN_ERR_INVALIDMEMORYTYPE);	/* Invalid type of memory. */
	ADD_PHP_CONST(GN_ERR_EMPTYLOCATION);		/* The given location is empty. */
	
	/* Format */
	ADD_PHP_CONST(GN_ERR_ENTRYTOOLONG); 		/* The given entry is too long */
	ADD_PHP_CONST(GN_ERR_WRONGDATAFORMAT);		/* Data format is not valid */
	ADD_PHP_CONST(GN_ERR_INVALIDSIZE);		/* Wrong size of the object */
	
	/* The following are here in anticipation of data call requirements. */
	ADD_PHP_CONST(GN_ERR_LINEBUSY);			/* Outgoing call requested reported line busy */
	ADD_PHP_CONST(GN_ERR_NOCARRIER);		/* No Carrier error during data call setup ? */
	
	/* The following value signals the current frame is unhandled */
	ADD_PHP_CONST(GN_ERR_UNHANDLEDFRAME);		/* The current frame isn't handled by the incoming function */
	ADD_PHP_CONST(GN_ERR_UNSOLICITED);		/* Unsolicited message received. */
	
	/* Other */
	ADD_PHP_CONST(GN_ERR_NONEWCBRECEIVED);		/* Attempt to read CB when no new CB received */
	ADD_PHP_CONST(GN_ERR_SIMPROBLEM);		/* SIM card missing or damaged */
	ADD_PHP_CONST(GN_ERR_CODEREQUIRED);		/* PIN or PUK code required */
	ADD_PHP_CONST(GN_ERR_NOTAVAILABLE);		/* The requested information is not available */
	
	/* Config */
	ADD_PHP_CONST(GN_ERR_NOCONFIG);			/* Config file cannot be found */
	ADD_PHP_CONST(GN_ERR_NOPHONE);			/* Either global or given phone section cannot be found */
	ADD_PHP_CONST(GN_ERR_NOLOG);			/* Incorrect logging section configuration */
	ADD_PHP_CONST(GN_ERR_NOMODEL);			/* No phone model specified */
	ADD_PHP_CONST(GN_ERR_NOPORT);			/* No port specified */
	
	ADD_PHP_CONST(GN_ERR_ASYNC);			/* The actual response will be sent asynchronously */

	return SUCCESS;
}
