/*

  $Id$

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Gnokii is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Gnokii is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with gnokii; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (C) 2006, 2012 by Daniele Forsi

  Prints the number of used and free phonebook locations.

  compile with
  gcc -Wall -o phonebook_status phonebook_status.c `pkg-config --libs gnokii`

  phonebook_status SM

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <gnokii.h>

/* prepare for i18n */
#define _(x) x

struct gn_statemachine *state = NULL;

void busterminate(void) {
	gn_lib_phone_close(state);
	gn_lib_phoneprofile_free(&state);
	gn_lib_library_free();
}

void businit(void) {
	gn_error	error;
	
	atexit(busterminate);
	
	error = gn_lib_phoneprofile_load(NULL, &state);
	if (GN_ERR_NONE == error) {
		error = gn_lib_phone_open(state);
	}
	
	if (GN_ERR_NONE != error) {
		fprintf(stderr, "%s\n", gn_error_print(error));
		exit(-1);
	}
}

void signal_handler(int signal) {
	(void)signal;
	exit(-2);
}

int main(int argc, char *argv[]) {
	gn_data		data;
	gn_error	error;
	gn_memory_status memorystatus = {0, 0, 0};
	unsigned int	i;
	gn_memory_type	mt[] = {
		GN_MT_ME, /* Internal memory of the mobile equipment */
		GN_MT_SM, /* SIM card memory */
		GN_MT_FD, /* Fixed dial numbers */
		GN_MT_ON, /* Own numbers */
		GN_MT_EN, /* Emergency numbers */
		GN_MT_DC, /* Dialled numbers */
		GN_MT_RC, /* Received numbers */
		GN_MT_MC, /* Missed numbers */
		GN_MT_LD, /* Last dialled */
		GN_MT_BD, /* Barred Dialling Numbers */
		GN_MT_SD, /* Service Dialling Numbers */
		GN_MT_MT, /* combined ME and SIM phonebook */
		GN_MT_TA, /* for compatibility only: TA=computer memory */
		GN_MT_CB, /* Currently selected memory */
		GN_MT_BM, /* Cell Broadcast Messages */
#ifdef GN_MT_MR
		GN_MT_MR, /* Message Recipients */
#endif
#ifdef GN_MT_CL
		GN_MT_CL, /* Call Log (DC + RC + MC) */
#endif
	};

	if (argc != 1) {
		fprintf(stderr, _("Usage: %s\nPurpose: print the number of used and free phonebook locations\n"), argv[0]);
		exit(-1);
	}

	businit();

	signal(SIGINT, signal_handler);

	gn_data_clear(&data);
	data.memory_status = &memorystatus;
	printf("used  free type\tname\n");
	for (i = 0; i < sizeof(mt) / sizeof(*mt); i++) {
		memorystatus.memory_type = mt[i];
		error = gn_sm_functions(GN_OP_GetMemoryStatus, &data, state);
		switch (error) {
		case GN_ERR_NONE:
			printf("%5d %5d %s\t%s\n", memorystatus.used, memorystatus.free, gn_memory_type2str(mt[i]), gn_memory_type_print(mt[i]));
			break;
		case GN_ERR_INVALIDMEMORYTYPE:
			break;
		default:
			fprintf(stderr, "%s\n", gn_error_print(error));
			exit(error);
		}
	}

	exit(error);
}
