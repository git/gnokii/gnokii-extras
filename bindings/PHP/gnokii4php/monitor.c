/* $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2009 by Daniele Forsi

  Monitoring related functions for gnokii PHP extension.

*/

#include "php_gnokii.h"

ZEND_DECLARE_MODULE_GLOBALS(gnokii)

/* this is almost an exact copy of the corresponding function from gnokii/gnokii-monitor.c */
PHP_FUNCTION(gnokii_netmonitor)
{
	zval *screen;
	char *m;
	long mode;
	gn_netmonitor nm;
	gn_data data;
	gn_error error;
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "z", &screen) == FAILURE) {
		RETURN_NULL();
	}

	switch (Z_TYPE_P(screen)) {
	case IS_STRING:
		m = Z_STRVAL_P(screen);
		if (!strcmp(m, "reset"))
			mode = 0xf0;
		else if (!strcmp(m, "off"))
			mode = 0xf1;
		else if (!strcmp(m, "field"))
			mode = 0xf2;
		else if (!strcmp(m, "devel"))
			mode = 0xf3;
		else if (!strcmp(m, "next"))
			mode = 0x00;
		else {
			/* since the PHP function parameter descriptor is "z",
			   strings containing only valid numbers aren't
			   automatically converted to long */
			convert_to_long(screen);
			mode = Z_LVAL_P(screen);
		}
		break;
	case IS_LONG:
		mode = Z_LVAL_P(screen);
		break;
	default:
		mode = -1;
	}
	/* this is slightly different from gnokii where 0xf0..0xf3 can't be entered directly as numbers */
	if (mode < 0 || mode > 0xf3) {
		LASTERROR(state, GN_ERR_INVALIDLOCATION);
		RETURN_NULL();
	}

	nm.field = mode;
	memset(&nm.screen, 0, 50);
	data.netmonitor = &nm;

	error = gn_sm_functions(GN_OP_NetMonitor, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	array_init(return_value);
	add_assoc_long(return_value, "field", nm.field);
	add_assoc_string(return_value, "screen", nm.screen, 1);
}
