#!/usr/bin/php
<?php // $Id$

/* clone of gnokii --getnetworkinfo */

require_once 'common.php';

function print_networkinfo()
{
	$networkinfo = gnokii_getnetworkinfo();

	if ($networkinfo) {
		echo sprintf("Network      : %s (%s)", $networkinfo['name'], $networkinfo['country']) . PHP_EOL;
		echo sprintf("Network code : %s", $networkinfo['code']) . PHP_EOL;
		echo sprintf("LAC          : %04x (%d)", $networkinfo['lac'], $networkinfo['lac']) . PHP_EOL;
		echo sprintf("Cell id      : %08x (%d)", $networkinfo['cell_id'], $networkinfo['cell_id']) . PHP_EOL;
	}

	return gnokii_lasterror();
}

/* get arguments */

script_init();

if ($argc != 1) {
	echo "Usage: {$argv[0]} [--config config] [--phone phone]" . PHP_EOL;
	exit(1);
}

$error = print_networkinfo();

script_terminate();

exit($error['code']);

