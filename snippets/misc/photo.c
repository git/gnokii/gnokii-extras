/*

  $Id$

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Gnokii is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Gnokii is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with gnokii; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (C) 2014 by Daniele Forsi

  Takes a photo with the camera built into the phone.
  This will likely work only with Series 40 3rd phones (see include/common.h for key values).

  compile with
  gcc -Wall -o photo photo.c `pkg-config --libs gnokii`

  usage:
  photo

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <gnokii.h>

/* prepare for i18n */
#define _(x) x

struct gn_statemachine *state = NULL;

void busterminate(void) {
	gn_lib_phone_close(state);
	gn_lib_phoneprofile_free(&state);
	gn_lib_library_free();
}

void businit(void) {
	gn_error	error;
	
	atexit(busterminate);
	
	error = gn_lib_phoneprofile_load(NULL, &state);
	if (GN_ERR_NONE == error) {
		error = gn_lib_phone_open(state);
	}
	
	if (GN_ERR_NONE != error) {
		fprintf(stderr, "%s\n", gn_error_print(error));
		exit(-1);
	}
}

void signal_handler(int signal) {
	(void)signal;
	exit(-2);
}

/* Definition copied from gnokii/gnokii-other.c */
static gn_error presskey(gn_data *data, struct gn_statemachine *state)
{
	gn_error error;
	error = gn_sm_functions(GN_OP_PressPhoneKey, data, state);
	if (error == GN_ERR_NONE)
		error = gn_sm_functions(GN_OP_ReleasePhoneKey, data, state);
	if (error != GN_ERR_NONE)
		fprintf(stderr, _("Failed to press key: %s\n"), gn_error_print(error));
	return error;
}

static gn_error press_camera_key(gn_data *data, struct gn_statemachine *state)
{
	data->key_code = GN_KEY_CAMERA;
	return presskey(data, state);
}

static gn_error press_red_phone_key(gn_data *data, struct gn_statemachine *state)
{
	data->key_code = GN_KEY_RED;
	return presskey(data, state);
}

int main(int argc, char *argv[]) {
	gn_data data;
	gn_error error;

	if (argc != 1) {
		fprintf(stderr, _("Usage: %s\nPurpose: take a photo with supported phones\n"), argv[0]);
		exit(-1);
	}

	businit();
	
	signal(SIGINT, signal_handler);

	gn_data_clear(&data);

	/* Go to the idle screen */
	error = press_red_phone_key(&data, state);
	if (error == GN_ERR_NONE) {
		/* Start the camera app and wait for it to be ready */
		error = press_camera_key(&data, state);
		sleep(2);
	}
	if (error == GN_ERR_NONE) {
		/* Take a picture and wait for it to be saved */
		error = press_camera_key(&data, state);
		sleep(2);
	}
	/* Go back to the idle screen */
	if (error == GN_ERR_NONE)
		error = press_red_phone_key(&data, state);

	exit(error);
}
