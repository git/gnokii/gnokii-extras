/*

  $Id$

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Gnokii is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Gnokii is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with gnokii; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (C) 2007 by Daniele Forsi

  An utility program to merge phonebook files using libgnokii functions.
  If duplicated entries are found the last one encountered takes precedence.
  This code is inspired by phonebook_convert.c

Example:

cat *.vcard | phonebook_merge --vcard >merged_phonebook

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gnokii.h>

#define TYPE_NONE  0
#define TYPE_LDIF  1
#define TYPE_RAW   2
#define TYPE_VCARD 3

#define PB_ALLOC 256
#define MAX_INPUT_LINE_LEN 1024

/* prepare for i18n */
#define _(x) x

typedef struct {
	gn_phonebook_entry *pb;
	int count;
	int last;
	int read;
} pb_struct;

int str2type(const char *str)
{
	if (!strcmp(str, "--ldif")) {
		return TYPE_LDIF;
	} else if (!strcmp(str, "--raw")) {
		return TYPE_RAW;
	} else if (!strcmp(str, "--vcard")) {
		return TYPE_VCARD;
	}

	return TYPE_NONE;
}

char *mt2str(gn_memory_type mt)
{
	switch (mt) {
	case GN_MT_ME:
		return "ME";
	case GN_MT_SM:
		return "SM";
	default:
		return "XX";
	}
}

int find_unused_location(pb_struct *pbs, gn_phonebook_entry *pe)
{
	int i = 0, location;

	/* a sequential search because the array is unordered */
	for (location = 0; location < pbs->count && i < pbs->count; ) {
		location++;
		for (i = 0; i < pbs->count; i++) {
			if ((pbs->pb[i].memory_type == pe->memory_type) && (pbs->pb[i].location == location)) {
				break;
			}
		}
	}

	return location;
}

/* TODO: if there is a reason because these entries couldn't be merged
   this function should return non zero, so that "src" is appended */
int merge_entries(gn_phonebook_entry *dest, gn_phonebook_entry *src)
{
	int i, j;

	strncpy(dest->name, src->name, GN_PHONEBOOK_NAME_MAX_LENGTH);
	strncpy(dest->number, src->number, GN_PHONEBOOK_NUMBER_MAX_LENGTH);

	/* update existing subentries of the same type and append new ones */
	for (j = 0; j < src->subentries_count; j++) {
		for (i = 0; i < dest->subentries_count; i++) {
			if ((src->subentries[j].entry_type == dest->subentries[i].entry_type) &&
			    (src->subentries[j].number_type == dest->subentries[i].number_type))
				break;
		}
		fprintf(stderr, "setting dest->subentry[%d]\n", i); /* DEBUG */
		memcpy(&dest->subentries[i], &src->subentries[j], sizeof(gn_phonebook_subentry));
		if (i == dest->subentries_count) dest->subentries_count++;
	}

	return 0;
}

int validate_entry(pb_struct *pbs, gn_phonebook_entry *pe)
{
	int i;

	/* a sequential search because the array is unordered and the search is for three items */
	for (i = 0; i < pbs->count; i++) {
		/* note: strcasecmp() isn't ANSI C */
		if (!strcasecmp(pbs->pb[i].name, pe->name)) {
			fprintf(stderr, _("Duplicated name %s\n"), pe->name);
			return merge_entries(&pbs->pb[i], pe);
		}

		if (!strcasecmp(pbs->pb[i].number, pe->number)) {
			fprintf(stderr, _("Duplicated number %s\n"), pe->number);
			return merge_entries(&pbs->pb[i], pe);
		}

		if ((pbs->pb[i].memory_type == pe->memory_type) && (pbs->pb[i].location == pe->location)) {
			if (pe->location) {
				fprintf(stderr, _("Duplicated location %d memory %d\n"), pe->location, pe->memory_type);
				/* will find a free location later to avoid stepping over entries yet to be read */
				pe->location = 0;
			}
			/* do not return now because this entry must be compared with the remaining names and numbers */
		}
	}

	return 1;
}

gn_error read_phonebook_from_fp(FILE *fp, int from_type, pb_struct *pbs)
{
	gn_phonebook_entry *new_pb, *pe;
	gn_error error = GN_ERR_NONE;
	char buffer[MAX_INPUT_LINE_LEN];

	for (;;) {
		/* expand array of gn_phonebook_entry when needed */
		if (pbs->last == pbs->count) {
			pbs->last = pbs->count + PB_ALLOC;
			new_pb = realloc(pbs->pb, pbs->last * sizeof(gn_phonebook_entry));
			if (new_pb == NULL) return GN_ERR_MEMORYFULL;
			pbs->pb = new_pb;
		}

		/* read until error, eof or array is full */
		for (pe = &pbs->pb[pbs->count]; pbs->count < pbs->last; ) {
			/* set default values which can be overriden by the conversion functions */
			memset(pe, 0, sizeof(gn_phonebook_entry));

			switch (from_type) {
			case TYPE_LDIF:
				if (gn_ldif2phonebook(fp, pe))
					error = GN_ERR_WRONGDATAFORMAT;
				break;
			case TYPE_RAW:
				if (gn_line_get(fp, buffer, MAX_INPUT_LINE_LEN))
					error = gn_file_phonebook_raw_parse(pe, buffer);
				break;
			case TYPE_VCARD:
				if (gn_vcard2phonebook(fp, pe))
					error = GN_ERR_WRONGDATAFORMAT;
				break;
			}
			/* FIXME: if file format is wrong gn_ldif2phonebook() and gn_vcard2phonebook() will read until EOF but next line will ignore that */
			if (feof(fp)) return GN_ERR_NONE;
			if (error != GN_ERR_NONE) break;

			pbs->read++;
			if (validate_entry(pbs, pe)) {
				pbs->count++;
				pe++;
			}
		}
	}

	return error;
}

gn_error read_phonebook_from_file(const char *filename, int from_type, pb_struct *pbs)
{
	FILE *fp;
	gn_error error = GN_ERR_UNKNOWN;

	fp = fopen(filename, "r");
	if (fp) {
		fprintf(stderr, _("Reading %s\n"), filename);
		error = read_phonebook_from_fp(fp, from_type, pbs);
		fclose(fp);
	} else {
		fprintf(stderr, _("Can't open file %s for reading!\n"), filename);
	}

	return error;
}

int main(int argc, char *argv[])
{
	gn_error error = GN_ERR_NONE;
	pb_struct pbs = {NULL, 0, 0, 0};
	int from_type, i;
	char buffer[MAX_INPUT_LINE_LEN];

	if ((argc < 2) ||
		(TYPE_NONE == (from_type = str2type(argv[1])))
	) {
		fprintf(stderr, _("Usage: %s type [file...]\n"), argv[0]);
		fprintf(stderr, _("Where type is one of --ldif, --raw or --vcard.\n"));
		fprintf(stderr, _("Merge phonebooks read from stdin or from given files.\n"));
		fprintf(stderr, _("Entries read first are updated with entries read last.\n"));
		return -1;
	}

	/* read and merge all files */
	if (argc == 2) {
		error = read_phonebook_from_fp(stdin, from_type, &pbs);
	} else {
		for (i = 2; (error == GN_ERR_NONE) && (i < argc); i++) {
			error = read_phonebook_from_file(argv[i], from_type, &pbs);
		}
	}

	/* output merged phonebooks */
	for (i = 0; (error == GN_ERR_NONE) && (i < pbs.count); i++) {
		if (!pbs.pb[i].location) {
			pbs.pb[i].location = find_unused_location(&pbs, &pbs.pb[i]);
		}
		switch (from_type) {
		case TYPE_LDIF:
			if (gn_phonebook2ldif(stdout, &pbs.pb[i]))
				error = GN_ERR_UNKNOWN;
			break;
		case TYPE_RAW:
			error = gn_file_phonebook_raw_write(stdout, &pbs.pb[i], mt2str(pbs.pb[i].memory_type));
			break;
		case TYPE_VCARD:
			sprintf(buffer, "%s%d", mt2str(pbs.pb[i].memory_type), pbs.pb[i].location);
			if (gn_phonebook2vcard(stdout, &pbs.pb[i], buffer))
				error = GN_ERR_UNKNOWN;
			break;
		}
	}

	fprintf(stderr, _("Read: %d Written: %d Merged: %d\n"), pbs.read, i, pbs.read - pbs.count);

	free(pbs.pb);

	if (error != GN_ERR_NONE) {
		fprintf(stderr, "%s\n", gn_error_print(error));
		return error;
	}

	return 0;
}
