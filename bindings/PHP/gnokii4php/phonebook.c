/* $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2008 by Daniele Forsi

  Phonebook related functions for gnokii PHP extension.

*/

#include "php_gnokii.h"

ZEND_DECLARE_MODULE_GLOBALS(gnokii)

PHP_FUNCTION(gnokii_getmemorystatus)
{
	char *memory_type_string;
	int memory_type_len;
	gn_memory_type memory_type;
	gn_data data;
	gn_memory_status memory_status;
	gn_error error;
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &memory_type_string, &memory_type_len) == FAILURE) {
		RETURN_NULL();
	}

	memory_type = gn_str2memory_type(memory_type_string);
	if (memory_type == GN_MT_XX) {
		LASTERROR(state, GN_ERR_INVALIDMEMORYTYPE);
		RETURN_NULL();
	}

	gn_data_clear(&data);
	memory_status.memory_type = memory_type;
	data.memory_status = &memory_status;

	error = gn_sm_functions(GN_OP_GetMemoryStatus, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	array_init(return_value);
	add_assoc_long(return_value, "used", memory_status.used);
	add_assoc_long(return_value, "free", memory_status.free);
}

PHP_FUNCTION(gnokii_getphonebook)
{
	char *memory_type_string;
	int memory_type_len;
	long location;
	gn_memory_type memory_type;
	gn_data data;
	gn_phonebook_entry pe;
	gn_error error;
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sl", &memory_type_string, &memory_type_len, &location) == FAILURE) {
		RETURN_NULL();
	}

	memory_type = gn_str2memory_type(memory_type_string);
	if (memory_type == GN_MT_XX) {
		LASTERROR(state, GN_ERR_INVALIDMEMORYTYPE);
		RETURN_NULL();
	}

	gn_data_clear(&data);
	memset(&pe, 0, sizeof(pe));
	pe.memory_type = memory_type;
	pe.location = location;
	data.phonebook_entry = &pe;

	error = gn_sm_functions(GN_OP_ReadPhonebook, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	array_init(return_value);
	add_assoc_string(return_value, "name", pe.name, 1);
	add_assoc_string(return_value, "number", pe.number, 1);
}

PHP_FUNCTION(gnokii_deletephonebook)
{
	char *memory_type_string;
	int memory_type_len;
	long location;
	gn_memory_type memory_type;
	gn_data data;
	gn_phonebook_entry pe;
	gn_error error;
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sl", &memory_type_string, &memory_type_len, &location) == FAILURE) {
		RETURN_NULL();
	}

	memory_type = gn_str2memory_type(memory_type_string);
	if (memory_type == GN_MT_XX) {
		LASTERROR(state, GN_ERR_INVALIDMEMORYTYPE);
		RETURN_NULL();
	}

	gn_data_clear(&data);
	memset(&pe, 0, sizeof(pe));
	pe.memory_type = memory_type;
	pe.location = location;
	data.phonebook_entry = &pe;

	error = gn_sm_functions(GN_OP_DeletePhonebook, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	RETURN_TRUE;
}

