/*

  $Id$

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Gnokii is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Gnokii is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with gnokii; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (C) 2006 by Daniele Forsi

  Prints the number of unread SM's.

  compile with
  gcc -Wall -o sms_status sms_status.c `pkg-config --libs gnokii`

  sms_status

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <gnokii.h>

/* prepare for i18n */
#define _(x) x

struct gn_statemachine *state = NULL;

void busterminate(void) {
	gn_lib_phone_close(state);
	gn_lib_phoneprofile_free(&state);
	gn_lib_library_free();
}

void businit(void) {
	gn_error	error;
	
	atexit(busterminate);
	
	error = gn_lib_phoneprofile_load(NULL, &state);
	if (GN_ERR_NONE == error) {
		error = gn_lib_phone_open(state);
	}
	
	if (GN_ERR_NONE != error) {
		fprintf(stderr, "%s\n", gn_error_print(error));
		exit(-1);
	}
}

void signal_handler(int signal) {
	(void)signal;
	exit(-2);
}

int main(int argc, char *argv[]) {
	gn_data		data;
	gn_error	error;
	gn_sms_status smsstatus = {0, 0, 0, 0, 0};

	if (argc != 1) {
		fprintf(stderr, _("Usage: %s\nPurpose: print the number of unread SM's and the total number\n"), argv[0]);
		exit(-1);
	}

	businit();
	
	signal(SIGINT, signal_handler);

	gn_data_clear(&data);
	data.sms_status = &smsstatus;
	error = gn_sm_functions(GN_OP_GetSMSStatus, &data, state);
	if (error == GN_ERR_NONE) {
		printf("%d %d\n", smsstatus.unread, smsstatus.number);
	}

	exit(error);
}
