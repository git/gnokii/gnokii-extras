#!/usr/bin/php
<?php // $Id$

/* clone of gnokii --getphonebook (only name and preferred number) */

require_once 'common.php';

function print_phonebook($memory_type, $start_number, $end_number)
{
	$no_error = Array('code' => GN_ERR_NONE, 'string' => '');
	$items_read = 0;

	$error = $no_error;
	for ($number = $start_number; $number <= $end_number; $number++) {
		$pe = gnokii_getphonebook($memory_type, $number);

		if ($pe) {
			$items_read++;
			/* TODO: pretty print the entry */
			print_r($pe);
		} else {
			$error = gnokii_lasterror();
			/* when reading only one entry always return the error */
			if ($start_number == $end_number) {
				return $error;
			}
			/* when reading more than one entry ignore empty locations
			 * but if all locations are empty the last $error is returned to the caller
			*/
			if ($error['code'] == GN_ERR_EMPTYLOCATION) {
				continue;
			}
			/* when reading to 'end' stop at the first invalid location */
			if (($error['code'] == GN_ERR_INVALIDLOCATION) && ($end_number == PHP_INT_MAX)) {
				if ($items_read == 0) {
					return Array('code' => GN_ERR_EMPTYLOCATION, 'string' => 'All locations are empty');
				}
				/* ignore the error if at least one location was read successfully */
				if ($items_read > 0) {
					return $no_error;
				}
			}
			/* all other errors are fatal */
			return $error;
		}
	}
	return $error;
}

script_init();

/* get arguments */

if (($argc != 3) && ($argc != 4)) {
	echo "Usage: {$argv[0]} [--config config] [--phone phone] memory_type start [end]" . PHP_EOL;
	exit(1);
}
$memory_type = $argv[1];
$start_number = $argv[2];
if ($argc == 3) {
	$end_number = $start_number;
} elseif ($argv[3] == 'end') {
	$end_number = PHP_INT_MAX;
} else {
	$end_number = $argv[3];
}

$error = print_phonebook($memory_type, $start_number, $end_number);

print_gnokii_error($error);

script_terminate();

exit($error['code']);

