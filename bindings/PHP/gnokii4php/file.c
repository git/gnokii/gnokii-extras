/* $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2009 by Daniele Forsi

  File related functions for gnokii PHP extension.

*/

#include "php_gnokii.h"

ZEND_DECLARE_MODULE_GLOBALS(gnokii)

/**
 * gn_file2array - populate a PHP associative array with file information
*/
static gn_file2array(gn_file *fi, zval *array){
	char datebuf[27];

	add_assoc_long(array, "filetype", fi->filetype);
	if (fi->id)
		add_assoc_string(array, "id", fi->id, 1);
	if (fi->name)
		add_assoc_string(array, "name", fi->name, 1);
	/* do NOT translate this date format */
	snprintf(datebuf, sizeof(datebuf), "%02d/%02d/%04d %02d:%02d:%02d %+03d00", \
		fi->day, fi->month, fi->year, \
		fi->hour, fi->minute, fi->second, \
		0 /* timezone */);
	datebuf[sizeof(datebuf)-1] = '\0';
	add_assoc_string(array, "datetime", datebuf, 1);
	add_assoc_long(array, "folderId", fi->folderId);
	add_assoc_long(array, "file_length", fi->file_length);
}

PHP_FUNCTION(gnokii_getfile)
{
	char *filename;
	int filename_len;
	gn_file fi;
	gn_data data;
	gn_error error;
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &filename, &filename_len) == FAILURE) {
		RETURN_NULL();
	}

	gn_data_clear(&data);
	data.file = &fi;
	memset(&fi, 0, sizeof(fi));
	snprintf(fi.name, 512, "%s", filename);
	/* filename too long? */
	if (fi.name[511]) {
		RETURN_NULL();
	}
	data.progress_indication = NULL;

	error = gn_sm_functions(GN_OP_GetFile, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	array_init(return_value);
	gn_file2array(&fi, return_value);
	add_assoc_stringl(return_value, "file", fi.file, fi.file_length, 1);
}

PHP_FUNCTION(gnokii_getfilelist)
{
	char *path;
	int path_len;
	gn_file_list fl;
	gn_data data;
	gn_error error;
	struct gn_statemachine *state;
	int i;
	zval *subarray;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &path, &path_len) == FAILURE) {
		RETURN_NULL();
	}

	gn_data_clear(&data);
	data.file_list = &fl;
	memset(&fl, 0, sizeof(fl));
	snprintf(fl.path, sizeof(fl.path), "%s", path);
	/* filename too long? */
	if (fl.path[sizeof(fl.path)]) {
		RETURN_NULL();
	}
	data.progress_indication = NULL;

	error = gn_sm_functions(GN_OP_GetFileList, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	array_init(return_value);
	for (i = 0; i < fl.file_count; i++) {
		MAKE_STD_ZVAL(subarray);
		array_init(subarray);
		gn_file2array(fl.files[i], subarray);
		add_next_index_zval(return_value, subarray);
		free(fl.files[i]);
	}
}

PHP_FUNCTION(gnokii_deletefile)
{
	char *filename;
	int filename_len;
	gn_file fi;
	gn_data data;
	gn_error error;
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &filename, &filename_len) == FAILURE) {
		RETURN_NULL();
	}

	gn_data_clear(&data);
	data.file = &fi;
	memset(&fi, 0, sizeof(fi));
	snprintf(fi.name, 512, "%s", filename);
	/* filename too long? */
	if (fi.name[511]) {
		RETURN_NULL();
	}
	data.progress_indication = NULL;

	error = gn_sm_functions(GN_OP_DeleteFile, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	RETURN_TRUE;
}

PHP_FUNCTION(gnokii_putfile)
{
	char *filename;
	int filename_len;
	char *file;
	int file_len;
	gn_file fi;
	gn_data data;
	gn_error error;
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ss", &filename, &filename_len, &file, &file_len) == FAILURE) {
		RETURN_NULL();
	}

	gn_data_clear(&data);
	data.file = &fi;
	memset(&fi, 0, sizeof(fi));
	snprintf(fi.name, 512, "%s", filename);
	/* filename too long? */
	if (fi.name[511]) {
		RETURN_NULL();
	}
	data.progress_indication = NULL;

	fi.file_length = file_len;
	fi.file = file;

	error = gn_sm_functions(GN_OP_PutFile, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	RETURN_TRUE;
}
