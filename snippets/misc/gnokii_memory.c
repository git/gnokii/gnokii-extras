/*

  $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2009 by Daniele Forsi

  Prints the description of the given error memory names.

  Compile and test:
  gcc -Wall gnokii_memory.c -o gnokii_memory $(pkg-config --libs gnokii) && ./gnokii_memory SM

*/

#include <stdio.h>

#include <gnokii.h>

int main(int argc, char *argv[]) {
	gn_error error = GN_ERR_NONE;
	int i;

	if ((argc < 2)) {
		printf("Usage %s memory_type...\n", argv[0]);
		return 1;
	}

	/* In this simple example gn_lib_init() is only needed to enable i18n */
	error = gn_lib_init();
	if (error != GN_ERR_NONE) {
		fprintf(stderr, "%s\n", gn_error_print(error));
	} else {
		for (i = 1; i < argc; i++) {
			printf("%s\n", gn_memory_type_print(gn_str2memory_type(argv[i])));
		}
	}

	return error;
}
