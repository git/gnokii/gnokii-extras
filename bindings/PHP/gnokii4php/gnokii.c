/* $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2006 by Daniele Forsi

  Basic libgnokii functions and PHP related code.

  Compile and test:

  phpize
  ./configure --enable-gnokii
  make && sudo make install
  php -r 'dl("gnokii.so"); gnokii_open(); print_r( gnokii_identify() ); print_r( gnokii_lasterror() ); print_r(gnokii_version());'

*/

#include "php_gnokii.h"

int le_gnokii_config_persist;

ZEND_DECLARE_MODULE_GLOBALS(gnokii)

static function_entry gnokii_functions[] = {
	/* from file.c */
	PHP_FE(gnokii_getfile, NULL)
	PHP_FE(gnokii_getfilelist, NULL)
	PHP_FE(gnokii_deletefile, NULL)
	PHP_FE(gnokii_putfile, NULL)
	/* from gnokii.c */
	PHP_FE(gnokii_open, NULL)
	PHP_FE(gnokii_close, NULL)
	PHP_FE(gnokii_identify, NULL)
	PHP_FE(gnokii_lasterror, NULL)
	PHP_FE(gnokii_version, NULL)
	PHP_FE(gnokii_reset, NULL)
	/* from monitor.c */
	PHP_FE(gnokii_netmonitor, NULL)
	/* from phonebook.c */
	PHP_FE(gnokii_deletephonebook, NULL)
	PHP_FE(gnokii_getmemorystatus, NULL)
	PHP_FE(gnokii_getphonebook, NULL)
	/* from sms.c */
	PHP_FE(gnokii_getsmsc, NULL)
	PHP_FE(gnokii_getsmsstatus, NULL)
	PHP_FE(gnokii_getsms, NULL)
	PHP_FE(gnokii_deletesms, NULL)
	PHP_FE(gnokii_sendsms, NULL)
	/* from other.c */
	PHP_FE(gnokii_listnetworks, NULL)
	PHP_FE(gnokii_getnetworkinfo, NULL)
	{NULL, NULL, NULL}
};

zend_module_entry gnokii_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
	STANDARD_MODULE_HEADER,
#endif
	PHP_GNOKII_EXT_NAME,
	gnokii_functions,
	PHP_MINIT(gnokii),
	PHP_MSHUTDOWN(gnokii),
	NULL,
	NULL,
	NULL,
#if ZEND_MODULE_API_NO >= 20010901
	PHP_GNOKII_EXT_VERSION,
#endif
	STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_GNOKII
ZEND_GET_MODULE(gnokii)
#endif

static int businit(char *config, char *phone) {
	TSRMLS_FETCH();
	gn_error error;
	struct gn_statemachine *state = NULL;

	php_error(E_NOTICE, __FUNCTION__);

	error = gn_lib_phoneprofile_load_from_file(config, phone, &state);
	if (error == GN_ERR_NONE) {
		error = gn_lib_phone_open(state);
		if (error == GN_ERR_NONE) {
			GNOKII_G(state) = state;

			return SUCCESS;
		}
		gn_lib_phoneprofile_free(&state);
	}

	php_error_docref(NULL TSRMLS_CC, E_WARNING, "%s", gn_error_print(error));
	GNOKII_G(state) = state;

	return FAILURE;
}

static int get_gnokii_config(char *config_name, int config_name_len, char *phone_name, int phone_name_len) {
	TSRMLS_FETCH();
	php_gnokii_config *gnokii_config;
	char *key;
	int key_len;
	list_entry *le = NULL, new_le;
	int result;
	zval *zgnokii_config;

	php_error(E_NOTICE, __FUNCTION__);

	MAKE_STD_ZVAL(zgnokii_config);

	/* Empty or missing strings means "use default value" */
	if (!config_name_len) config_name = NULL;
	if (!phone_name_len) phone_name = NULL;

	/* Look for an established resource */
	key_len = spprintf(&key, 0, "gnokii_config_%s_%s", config_name, phone_name);
	result = zend_hash_find(&EG(persistent_list), key, key_len + 1, (void **) &le);
	if (result == SUCCESS) {
		php_error_docref(NULL TSRMLS_CC, E_NOTICE, "reopen %s %s %p", config_name, phone_name, le);
		/* An entry for this config already exists */
		if (le && le->ptr) {
			gnokii_config = (php_gnokii_config *) le->ptr;
			GNOKII_G(state) = gnokii_config->state;
		} else {
			result = FAILURE;
		}
	} else {
		php_error_docref(NULL TSRMLS_CC, E_NOTICE, "open %s %s", config_name, phone_name);
		/* Try to open the phone */
		result = businit(config_name, phone_name);
		if (result == SUCCESS) {
			/* New config, allocate a structure */
			gnokii_config = pemalloc(sizeof(php_gnokii_config), 1);

			if (config_name_len) {
				gnokii_config->config_name = pemalloc(config_name_len + 1, 1);
				memcpy(gnokii_config->config_name, config_name, config_name_len + 1);
			} else {
				gnokii_config->config_name = NULL;
			}

			if (phone_name_len) {
				gnokii_config->phone_name = pemalloc(phone_name_len + 1, 1);
				memcpy(gnokii_config->phone_name, phone_name, phone_name_len + 1);
			} else {
				gnokii_config->phone_name = NULL;
			}

			gnokii_config->state = GNOKII_G(state);

			ZEND_REGISTER_RESOURCE(zgnokii_config, gnokii_config, le_gnokii_config_persist);

			/* Store a reference in the persistent list */
			new_le.ptr = gnokii_config;
			new_le.type = le_gnokii_config_persist;
			result = zend_hash_add(&EG(persistent_list), key, key_len + 1, &new_le, sizeof(list_entry), NULL);
			//php_error_docref(NULL TSRMLS_CC, E_WARNING, "result %s\n", result);
		}
	}

	efree(key);
	FREE_ZVAL(zgnokii_config);

	return result;
}

static int busterminate(void) {
	TSRMLS_FETCH();
	struct gn_statemachine *state = NULL;

	php_error(E_NOTICE, __FUNCTION__);
	state = GNOKII_G(state);

	if (state) {
		gn_lib_phone_close(state);
		gn_lib_phoneprofile_free(&state);
	}
	gn_lib_library_free();

	GNOKII_G(state) = state;

	return SUCCESS;
}

static void php_gnokii_config_persist_dtor(zend_rsrc_list_entry *rsrc TSRMLS_DC)
{
	php_gnokii_config *gnokii_config = (php_gnokii_config *) rsrc->ptr;

	php_error(E_NOTICE, __FUNCTION__);
	if (gnokii_config) {
		if (gnokii_config->config_name) {
			pefree(gnokii_config->config_name, 1);
		}
		if (gnokii_config->phone_name) {
			pefree(gnokii_config->phone_name, 1);
		}
		pefree(gnokii_config, 1);
	}
}

static void php_gnokii_init_globals(zend_gnokii_globals *gnokii_globals)
{
	TSRMLS_FETCH();

	gnokii_globals->state = NULL;
}

PHP_MINIT_FUNCTION(gnokii)
{
	le_gnokii_config_persist = zend_register_list_destructors_ex(NULL, php_gnokii_config_persist_dtor, PHP_GNOKII_CONFIG_RES_NAME, module_number);
	ZEND_INIT_MODULE_GLOBALS(gnokii, php_gnokii_init_globals, NULL);
	php_error_docref(NULL TSRMLS_CC, E_NOTICE, "MINIT");
	return php_gnokii_const_init(module_number);
}

PHP_MSHUTDOWN_FUNCTION(gnokii)
{
	php_error_docref(NULL TSRMLS_CC, E_NOTICE, "MSHUTDOWN");

	return busterminate();
}

PHP_FUNCTION(gnokii_open)
{
	char *config = NULL, *phone = NULL;
	int config_len = 0, phone_len = 0;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "|ss", &config, &config_len, &phone, &phone_len) == FAILURE) {
		RETURN_NULL();
	}

	RETURN_BOOL(get_gnokii_config(config, config_len, phone, phone_len) == SUCCESS);
}

PHP_FUNCTION(gnokii_close)
{
	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "") == FAILURE) {
		RETURN_NULL();
	}

	if (busterminate() == SUCCESS) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}

PHP_FUNCTION(gnokii_identify)
{
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "") == FAILURE) {
		RETURN_NULL();
	}

	array_init(return_value);
	add_assoc_string(return_value, "imei", (char *) gn_lib_get_phone_imei(state), 1);
	add_assoc_string(return_value, "manufacturer", (char *) gn_lib_get_phone_manufacturer(state), 1);
	add_assoc_string(return_value, "model", (char *) gn_lib_get_phone_model(state), 1);
	add_assoc_string(return_value, "product_name", (char *) gn_lib_get_phone_product_name(state), 1);
	add_assoc_string(return_value, "revision", (char *) gn_lib_get_phone_revision(state), 1);
}

PHP_FUNCTION(gnokii_lasterror)
{
	gn_error error;
	struct gn_statemachine *state;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "") == FAILURE) {
		RETURN_NULL();
	}

	state = GNOKII_G(state);

	error = gn_lib_lasterror(state);

	array_init(return_value);
	add_assoc_long(return_value, "code", error);
	add_assoc_string(return_value, "string", gn_error_print(error), 1);
}

PHP_FUNCTION(gnokii_version)
{
	struct gn_statemachine *state;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "") == FAILURE) {
		RETURN_NULL();
	}

	array_init(return_value);
	add_assoc_string(return_value, "libgnokii_version", LIBGNOKII_VERSION_STRING, 1);
	add_assoc_string(return_value, "extension_version", PHP_GNOKII_EXT_VERSION, 1);
	add_assoc_string(return_value, "abi_version", PHP_GNOKII_ABI_VERSION, 1);
}

PHP_FUNCTION(gnokii_reset)
{
#define RESET_TYPE_SOFT 0x03
#define RESET_TYPE_HARD 0x04

	gn_data data;
	zend_bool hard_reset;
	gn_error error;
	struct gn_statemachine *state;

	FETCH_NOT_NULL(state);

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "b", &hard_reset) == FAILURE) {
		RETURN_NULL();
	}

	gn_data_clear(&data);
	data.reset_type = (hard_reset ? RESET_TYPE_HARD : RESET_TYPE_SOFT);

	error = gn_sm_functions(GN_OP_Reset, &data, state);
	LASTERROR(state, error);
	if (error != GN_ERR_NONE) {
		RETURN_NULL();
	}

	RETURN_TRUE;
}
