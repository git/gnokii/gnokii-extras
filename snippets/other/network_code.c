/*

  $Id$

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (c) 2006 by Daniele Forsi

  Prints the MCC and the MNC corresponding to the given network name (operator)
  and country name, both case insensitive.
  Prints "undefined" if the name is not found.

  gcc -Wall network_code.c -o network_code $(pkg-config --libs gnokii)
  network_code vodafone italy
  network_code VODAFONE GERMANY
*/

#include <stdio.h>

#include <gnokii.h>

int main(int argc, char *argv[]) {
	char *country, *code;

	if (argc == 3) {
		country = argv[2];
	} else {
		printf("Usage: %s network_name country_name\n", argv[0]);
		return 1;
	}
	
	code = gn_network_code_find(argv[1], country);
	printf("%s\n", code);

	return 0;
}
