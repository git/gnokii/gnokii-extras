/*

  $Id$

  G N O K I I

  A Linux/Unix toolset and driver for the mobile phones.

  This file is part of gnokii.

  Gnokii is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  Gnokii is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with gnokii; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  Copyright (C) 2007, 2008 by Daniele Forsi

  An utility program to convert mms formats using libgnokii functions.
  This code is inspired by gnokii-mms.c

Example:

mms_convert infile1 --txt outfile1 infile2 --mime outfile2

*/
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <gnokii.h>

/* prepare for i18n */
#define _(x) x

/*
 * Allocates a @buffer of @length bytes and fills it with contents read from @fp,
 * returns size of buffer in @length. Caller needs to free() @buffer.
 */
gn_error file_get_contents_fp(FILE *fp, unsigned char **buffer, size_t *length)
{
	struct stat st;

	if (fstat(fileno(fp), &st))
		 return GN_ERR_FAILED;
	*buffer = malloc(st.st_size);
	if (!*buffer)
		 return GN_ERR_MEMORYFULL;
	*length = fread(*buffer, sizeof(char), st.st_size, fp);
	if (*length != (size_t) st.st_size) {
		perror(NULL);
		return GN_ERR_FAILED;
	}

	return GN_ERR_NONE;
}

/*
 * Allocates a @buffer of @length bytes and fills it with contents read from @filename,
 * returns size of buffer in @length. Caller needs to free() @buffer.
 */
gn_error file_get_contents(const char *filename, unsigned char **buffer, size_t *length)
{
	FILE *fp;
	gn_error error;

	fp = fopen(filename, "r");
	if (!fp) {
		perror(filename);
		return GN_ERR_FAILED;
	}
	error = file_get_contents_fp(fp, buffer, length);
	fclose(fp);

	return error;
}

/*
 * Writes to @filename @length bytes from @buffer
 */
gn_error file_put_contents(const char *filename, unsigned char **buffer, size_t *length)
{
	FILE *fp;
	gn_error error = GN_ERR_NONE;
	size_t written;

	fp = fopen(filename, "w");
	if (!fp) {
		perror(filename);
		return GN_ERR_FAILED;
	}
	written = fwrite(*buffer, sizeof(char), *length, fp);
	if (written != *length) {
		perror(filename);
		error = GN_ERR_FAILED;
	}
	fclose(fp);

	return error;
}

/*
 * Converts a string @str to a #gn_mms_format enum
 */
gn_mms_format str2type(const char *str)
{
	if (!strcmp(str, "--mime")) {
		return GN_MMS_FORMAT_MIME;
	} else if (!strcmp(str, "--pdu")) {
		return GN_MMS_FORMAT_PDU;
	} else if (!strcmp(str, "--raw")) {
		return GN_MMS_FORMAT_RAW;
	} else if (!strcmp(str, "--txt")) {
		return GN_MMS_FORMAT_TEXT;
	}

	return GN_MMS_FORMAT_UNKNOWN;
}

gn_error invalid_output_format(const char *arg)
{
	fprintf(stderr, _("Unknown output format %s\n"), arg);
	return GN_ERR_NOTSUPPORTED;
}

gn_error print_error(gn_error error)
{
	fprintf(stderr, "%s\n", gn_error_print(error));
	return error;
}

int main(int argc, char *argv[])
{
	int i;
	unsigned char *buffer;
	size_t length;
	gn_error error = GN_ERR_NONE;
	gn_mms *source_mms, *dest_mms;
	gn_mms_format to_type;

	if (argc == 1 || (argc - 1) % 3) {
		fprintf(stderr, _("Usage: %s {infile outtype outfile}...\n"), argv[0]);
		fprintf(stderr, _("Where outtype is one of --mime, --pdu, --raw or --txt.\n"));
		fprintf(stderr, _("Format of input file is autodetected. Multiple files can be converted.\n"));
		return -1;
	}

	for (i = 1; i < argc; i += 3) {
		to_type = str2type(argv[i + 1]);
		if (to_type == GN_MMS_FORMAT_UNKNOWN) {
			return invalid_output_format(argv[i + 1]);
		}
			
		error = file_get_contents(argv[i], &buffer, &length);
		if (error != GN_ERR_NONE) {
			return print_error(error);
		}

		error = gn_mms_alloc(&source_mms);
		if (error == GN_ERR_NONE) {
			source_mms->buffer = buffer;
			source_mms->buffer_length = length;
			source_mms->buffer_format = GN_MMS_FORMAT_UNKNOWN;

			error = gn_mms_alloc(&dest_mms);
			if (error == GN_ERR_NONE) {
				dest_mms->buffer_format = to_type;

				error = gn_mms_convert(source_mms, dest_mms);
				if (error == GN_ERR_NONE) {
					error = file_put_contents(argv[i + 2], &dest_mms->buffer, &dest_mms->buffer_length);
				}

				gn_mms_free(dest_mms);
			}
			gn_mms_free(source_mms);
		}
		if (error != GN_ERR_NONE)
			return print_error(error);
	}
	return error;
}

