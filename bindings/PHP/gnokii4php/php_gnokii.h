/* $Id$ */

#ifndef PHP_GNOKII_H
#define PHP_GNOKII_H 1

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define PHP_GNOKII_EXT_VERSION "0.0.12"
#define PHP_GNOKII_EXT_NAME "gnokii"
#define PHP_GNOKII_ABI_VERSION "1.1.0"

#ifdef ZTS
#include "TSRM.h"
#endif

#include "php.h"
#include "gnokii.h"

ZEND_BEGIN_MODULE_GLOBALS(gnokii)
	struct gn_statemachine *state;
ZEND_END_MODULE_GLOBALS(gnokii)

#ifdef ZTS
#define GNOKII_G(v) TSRMG(gnokii_globals_id, zend_gnokii_globals *, v)
#else
#define GNOKII_G(v) (gnokii_globals.v)
#endif

typedef struct _php_gnokii_config {
	char *config_name;
	char *phone_name;
	struct gn_statemachine *state;
} php_gnokii_config;

#define PHP_GNOKII_CONFIG_RES_NAME "gnokii config data"

PHP_MINIT_FUNCTION(gnokii);
PHP_MSHUTDOWN_FUNCTION(gnokii);
PHP_RINIT_FUNCTION(gnokii);

/* from file.c */
PHP_FUNCTION(gnokii_getfile);
PHP_FUNCTION(gnokii_getfilelist);
PHP_FUNCTION(gnokii_deletefile);
PHP_FUNCTION(gnokii_putfile);
/* from gnokii.c */
PHP_FUNCTION(gnokii_open);
PHP_FUNCTION(gnokii_close);
PHP_FUNCTION(gnokii_identify);
PHP_FUNCTION(gnokii_lasterror);
PHP_FUNCTION(gnokii_version);
PHP_FUNCTION(gnokii_reset);
/* from monitor.c */
PHP_FUNCTION(gnokii_netmonitor);
/* from phonebook.c */
PHP_FUNCTION(gnokii_deletephonebook);
PHP_FUNCTION(gnokii_getphonebook);
PHP_FUNCTION(gnokii_getmemorystatus);
/* from sms.c */
PHP_FUNCTION(gnokii_getsmsc);
PHP_FUNCTION(gnokii_getsmsstatus);
PHP_FUNCTION(gnokii_getsms);
PHP_FUNCTION(gnokii_deletesms);
PHP_FUNCTION(gnokii_sendsms);
/* from other.c */
PHP_FUNCTION(gnokii_listnetworks);
PHP_FUNCTION(gnokii_getnetworkinfo);

extern zend_module_entry gnokii_module_entry;
#define phpext_gnokii_ptr &gnokii_module_entry

#define FETCH_NOT_NULL(x) x = GNOKII_G(x); if (!x) RETURN_NULL();

#define LASTERROR(state,nr)	((state->lasterror = nr)) /* do not delete the double brackets! */

#endif

